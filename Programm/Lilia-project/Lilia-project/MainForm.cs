﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.cookies;
using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    
    public partial class MainForm : Form
    {
        List<Trip> tripList = new List<Trip>();
        List<Class> classList = new List<Class>();
        List<Siting> sitingList = new List<Siting>();
        List<MyTrip> mytripList = new List<MyTrip>();
        List<AirCompani> myAirCompList = new List<AirCompani>();

        ClassObject classObj;
        AirCompani airCompani = new AirCompani();
        Timer tmForward;
        Timer tmBack;

        public User user = new User();
        public Owner owner = new Owner();
        public Role role = Role.None;
        
        public MainForm()
        {
            InitializeComponent();

            tmForward = new System.Windows.Forms.Timer();
            tmForward.Tick += new EventHandler(MenuForward);          
            tmForward.Interval = 1;

            this.Width++;
        }     
        
        private void CloseAllPanel()
        {
            this.MainPanel.Visible = false;
            this.MyAirCompPanel.Visible = false;
            this.CreateTripPanel.Visible = false;
            this.MyTripsPanelM.Visible = false;
            this.BookingListPanel.Visible = false;
            this.TripsStatistic.Visible = false;
        }

        private void TripsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }



        private void label8_Click(object sender, EventArgs e)
        {

        }

        //styles
        public void DefaultStyle()
        {
            CloseAllPanel();

            ClearAirCompaniPanel();
            ClearBookingListPanel();
            ClearCreateTripsForms();
            ClearTripsPanel();
            ClearMyTripsPanel();
            ClearTripsStatisticsPanel();

            this.TripsStatistic.Visible = false;
            this.label67.Visible = false;
            this.label65.Visible = false;
            this.tabControl1.Visible = false;
            this.label42.Visible = false;
            this.textBox3.Visible = false;
            this.MainPanel.Visible = false;
            this.label5.Visible = false;
            this.label6.Visible = false;
            this.label9.Visible = false;
            this.label10.Visible = false;
            this.label11.Visible = false;
            this.BuyTicketMainPanelButton.Visible = false;
            this.BookSiatButton.Visible = false;
        }
        public void Guest()
        {
            this.MainPanel.Visible = true;
            this.label5.Visible = true;
        }
        public void UserStyle()
        {
            ClearMyTripsPanel();
            ClearTripsPanel();
            this.label65.Visible = true;
            this.MainPanel.Visible = true;
            this.BookSiatButton.Visible = true;
            this.label6.Visible = true;
            this.label5.Visible = true;
        }
        public void ManagerStyle()
        {
            ClearCreateTripsForms();
            this.TripsStatistic.Visible = true;
            this.label67.Visible = true;
            this.label65.Visible = true;
            this.CreateTripPanel.Visible = true;
            this.label10.Visible = true;
        }
        public void PayMasterStyle()
        {
            ClearTripsPanel();
            ClearBookingListPanel();
            this.label65.Visible = true;
            this.label42.Visible = true;
            this.textBox3.Visible = true;
            this.label11.Visible = true;
            this.BuyTicketMainPanelButton.Visible = true;
            this.MainPanel.Visible = true;
            this.label5.Visible = true;
        }
        public void AdminStyle()
        {
            ClearAirCompaniPanel();
            this.label65.Visible = true;
            this.TripsStatistic.Visible = true;
            this.MyAirCompPanel.Visible = true;
            this.label9.Visible = true;
            this.label67.Visible = true;
        }

       
    }
}
