﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        private void SearchTripsButton_Click(object sender, EventArgs e)
        {
            tripList = Sends.GetTrips(
                this.WhenceTextBox.Text,
                this.WhereTextBox.Text,
                this.DateMonthCalendar.SelectionRange.Start.ToShortDateString(),
                this.DateUpgradeCheckBox.Checked,
                false
            );
         
            fillDataGridView();

            ResetAll();
        }
        private void TripsDataGridView_CellContentClick()
        {
            if (role == Role.None)
                return;

            airCompani = Sends.GetAirCompani(tripList[this.TripsDataGridView.SelectedRows[0].Index].id_compani + "");
            classList = Sends.GetClasses(tripList[this.TripsDataGridView.SelectedRows[0].Index].id + "");

            ResetAll();

            this.NameAirCompaniMainPanelTextBox.Text = airCompani.name;
            this.DescrAirCompaniMainPanelTextBox.Text = airCompani.discription;

            
            fillClassesDataGridView();

            this.BookingResultLabel1.Text = "";
        }
        private void ClassDataGridView_CellContentClick()
        {
            if (role == Role.None)
                return;

            sitingList = Sends.GetSits(classList[this.ClassesDataGridView.SelectedRows[0].Index].id + "");
            fillSitingDataGridView();
        }
        private void BookSiatButton_Click(object sender, EventArgs e)
        {
            if (this.SiatsDataGridView.GetCellCount(DataGridViewElementStates.Selected) == 0)
                return;

            SitingSend sendList = new SitingSend();

            for (int i = 0; i < this.SiatsDataGridView.SelectedCells.Count; i++)
            {
                sendList.id_sit.Add(sitingList[this.SiatsDataGridView.SelectedCells[i].RowIndex].id);
            }

            sendList.id_user = this.user.id;

            int res = Sends.BookSits(sendList);

            if (res == 1)
            {
                this.BookingResultLabel1.ForeColor = Color.ForestGreen;
                this.BookingResultLabel1.Text = "Sits are booked";
            }
            else if (res == 2)
            {
                this.BookingResultLabel1.ForeColor = Color.Red;
                this.BookingResultLabel1.Text = "Sits were booked another user, please update list";
            }
        }
        private void SiatsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            this.BookingResultLabel.Text = "";
        }

        private void TripsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (this.TripsDataGridView.GetCellCount(DataGridViewElementStates.Selected) != 5)
            {
                this.TripsDataGridView.ClearSelection();
                this.ClassesDataGridView.ClearSelection();
                this.SiatsDataGridView.ClearSelection();
            }
            else
            {
                TripsDataGridView_CellContentClick();
            }
        }
        private void ClassesDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (this.ClassesDataGridView.GetCellCount(DataGridViewElementStates.Selected) != 2)
            {
                this.ClassesDataGridView.ClearSelection();
                this.SiatsDataGridView.ClearSelection();
            }
            else
            {
                ClassDataGridView_CellContentClick();
            }
        }
        private void BuyTicketMainPanelButton_Click(object sender, EventArgs e)
        {
            if (this.SiatsDataGridView.GetCellCount(DataGridViewElementStates.Selected) == 0)
                return;
            if(this.textBox3.Text.Trim() == "")
            {
                this.BookingResultLabel1.ForeColor = Color.Red;
                this.BookingResultLabel1.Text = "Fill pasport form";
                return;
            }

            SitingSend sendList = new SitingSend();

            for (int i = 0; i < this.SiatsDataGridView.SelectedCells.Count; i++)
            {
                sendList.id_sit.Add(sitingList[this.SiatsDataGridView.SelectedCells[i].RowIndex].id);
            }
            
            if (Sends.BuyTickets(sendList))
            {
                this.BookingResultLabel1.ForeColor = Color.ForestGreen;
                this.BookingResultLabel1.Text = "Sits are bought";
            }
            else
            {
                this.BookingResultLabel1.ForeColor = Color.Red;
                this.BookingResultLabel1.Text = "Sits were bougth another user, please update list";
            }
        }

        private void fillDataGridView()
        {
            this.TripsDataGridView.Rows.Clear();

            if (tripList == null)
                return;

            for (int i = 0; i < tripList.Count; i++)
            {
                DateTime date;
                this.TripsDataGridView.Rows.Add();

                this.TripsDataGridView[0, i].Value = tripList[i].fromcity;
                this.TripsDataGridView[1, i].Value = tripList[i].wherecity;

                date = DateTime.Parse(tripList[i].depature_date);
                this.TripsDataGridView[2, i].Value = date.ToShortDateString() + "/" + date.ToShortTimeString();

                date = DateTime.Parse(tripList[i].arrival_date);
                this.TripsDataGridView[3, i].Value = date.ToShortDateString() + "/" + date.ToShortTimeString();

                this.TripsDataGridView[4, i].Value = tripList[i].aircraft;
            }
        }
        private void fillClassesDataGridView()
        {
            this.ClassesDataGridView.Rows.Clear();

            for (int i = 0; i < classList.Count; i++)
            {
                this.ClassesDataGridView.Rows.Add();

                this.ClassesDataGridView[0, i].Value = classList[i].name;
                this.ClassesDataGridView[1, i].Value = classList[i].price;
            }
        }
        private void fillSitingDataGridView()
        {
            this.SiatsDataGridView.Rows.Clear();

            for (int i = 0; i < sitingList.Count; i++)
            {
                this.SiatsDataGridView.Rows.Add();
                this.SiatsDataGridView[0, i].Value = sitingList[i].number;
            }
        }

        private void ResetAll()
        {
            this.BookingResultLabel.Text = "";
            this.ClassesDataGridView.Rows.Clear();
            this.SiatsDataGridView.Rows.Clear();
            this.NameAirCompaniMainPanelTextBox.Text = "";
            this.DescrAirCompaniMainPanelTextBox.Text = "";
        }
        private void ClearTripsPanel()
        {
            ResetAll();
            this.TripsDataGridView.Rows.Clear();
            this.BookingResultLabel1.Text = "";
            this.WhereTextBox.Text = "";
            this.WhenceTextBox.Text = "";
        }
    }
}
