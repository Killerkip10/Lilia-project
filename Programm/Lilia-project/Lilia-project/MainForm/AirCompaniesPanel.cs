﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {

        List<int> companiStatList = new List<int>();
        List<int> staffStatList = new List<int>();
        List<User> staffList = new List<User>();

        public void MyAirCompDataGridView_CellContentClick()
        {
            int month = 1;

            if (radioButton6.Checked)
                month = 1;
            if (radioButton7.Checked)
                month = 3;
            if (radioButton8.Checked)
                month = 6;

            companiStatList = Sends.GetAirCompaniStat(myAirCompList[this.MyAirCompDataGridView.SelectedRows[0].Index].id + "", month);
            staffStatList = Sends.GetStaffStat(myAirCompList[this.MyAirCompDataGridView.SelectedRows[0].Index].id + "", month);

            this.chart3.Series["Series1"].Points.Clear();

            this.chart3.Series["Series1"].Points.Add(companiStatList[0]);
            this.chart3.Series["Series1"].Points[0].Color = Color.Tomato;
            this.chart3.Series["Series1"].Points[0].LegendText = companiStatList[0] + " - Waiting";

            this.chart3.Series["Series1"].Points.Add(companiStatList[1]);
            this.chart3.Series["Series1"].Points[1].Color = Color.LightGreen;
            this.chart3.Series["Series1"].Points[1].LegendText = companiStatList[1] + " - Realize";


            this.chart4.Series["Series1"].Points.Clear();

            this.chart4.Series["Series1"].Points.Add(staffStatList[0] - staffStatList[1]);
            this.chart4.Series["Series1"].Points[0].Color = Color.LightBlue;
            this.chart4.Series["Series1"].Points[0].LegendText = (staffStatList[0] - staffStatList[1]) + " - Internet";

            this.chart4.Series["Series1"].Points.Add(staffStatList[1]);
            this.chart4.Series["Series1"].Points[1].Color = Color.LightGreen;
            this.chart4.Series["Series1"].Points[1].LegendText = staffStatList[1] + " - Cashbox";

        }

        private void MyAirCompDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (this.MyAirCompDataGridView.GetCellCount(DataGridViewElementStates.Selected) != 2)
            {
                this.MyAirCompDataGridView.ClearSelection();
            }
            else
            {
                MyAirCompDataGridView_CellContentClick();
            }
        }
        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            if(this.dataGridView3.GetCellCount(DataGridViewElementStates.Selected) != 5)
            {
                this.dataGridView3.ClearSelection();
            }
            else
            {
                
            }
        }

        private void CreateAirCompButton_Click(object sender, EventArgs e)
        {
            if (this.NameAirCompTextBox.Text.Trim() == "" || this.DescriptionAirCompTextBox.Text.Trim() == "")
            {
                this.label19.Text = "Not all forms are filled";
                this.label19.ForeColor = Color.Red;
                return;
            }

            this.label19.Text = "";

            if (Sends.CreateAirCompani(new CreateAirCompSend(this.NameAirCompTextBox.Text, this.DescriptionAirCompTextBox.Text)))
            {
                this.label19.Text = "Air compani is registrated";
                this.label19.ForeColor = Color.ForestGreen;

                this.NameAirCompTextBox.Text = "";
                this.DescriptionAirCompTextBox.Text = "";
            }
            else
            {
                this.label19.Text = "Error";
                this.label19.ForeColor = Color.Red;
                this.label19.Visible = true;
            }
        }
        private void GetMyAirCompButton_Click(object sender, EventArgs e)
        {
            myAirCompList = Sends.GetMyAirCompani();
            fillMyAirCompaniDataGridView();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            staffList = Sends.GetStaff();
            filldataGridView3();
        }

        private void fillMyAirCompaniDataGridView()
        {
            this.MyAirCompDataGridView.Rows.Clear();

            for (int i = 0; i < myAirCompList.Count; i++)
            {
                this.MyAirCompDataGridView.Rows.Add();
                this.MyAirCompDataGridView[0, i].Value = myAirCompList[i].name;
                this.MyAirCompDataGridView[1, i].Value = myAirCompList[i].discription;
            }
        }
        private void filldataGridView3()
        {
            if (staffList == null)
                return;

            this.dataGridView3.Rows.Clear();

            for(int i = 0; i < staffList.Count; i++)
            {
                this.dataGridView3.Rows.Add();

                this.dataGridView3[0, i].Value = staffList[i].name;
                this.dataGridView3[1, i].Value = staffList[i].surname;
                this.dataGridView3[2, i].Value = staffList[i].email;
                this.dataGridView3[3, i].Value = staffList[i].phone;

                switch (staffList[i].role) {
                    case "3":
                        {
                            this.dataGridView3[4, i].Value = "manager";
                        }
                        break;
                    case "4":
                        {
                            this.dataGridView3[4, i].Value = "paymaster";
                        }break;

                }
            }
        }

        private void ClearAirCompaniPanel()
        {
            this.NameAirCompTextBox.Text = "";
            this.DescriptionAirCompTextBox.Text = "";
            this.label19.Text = "";
            this.MyAirCompDataGridView.Rows.Clear();
            this.dataGridView3.Rows.Clear();
            this.chart3.Series["Series1"].Points.Clear();
            this.chart4.Series["Series1"].Points.Clear();
        }
    }
}
