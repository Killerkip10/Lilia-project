﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        private void fillMyAircraftComboBox()
        {
            this.AirCompaniesComboBox.Items.Clear();

            for (int i = 0; i < myAirCompList.Count; i++)
            {
                this.AirCompaniesComboBox.Items.Add(myAirCompList[i].name);
            }
        }
        private void CreateTripAddClassButton_Click(object sender, EventArgs e)
        {
            classObj.AddPanel();
        }
        private void CreateTripButton_Click(object sender, EventArgs e)
        {
            if (this.AirCompaniesComboBox.SelectedIndex.ToString() == "-1")
            {
                InvalidCreateTrip(false);
                return;
            }
            if (!classObj.CheckValid())
            {
                InvalidCreateTrip(false);
                return;
            }
            CreateTripSend trip = new CreateTripSend(
                new Trip(
                    myAirCompList[Convert.ToInt32(this.AirCompaniesComboBox.SelectedIndex.ToString())].id,
                    this.CreateTripWhenceTextBox.Text,
                    this.CreateTripWhereTextBox.Text,
                    this.DepatureDateTimePicker.Value.ToUniversalTime().ToString(),
                    this.ArrivalDateTimePicker1.Value.ToUniversalTime().ToString(),
                    this.CreateTripAircraftTextBox.Text
                ),
                classObj.GetClassList()
            );
            Sends.CreateTrip(trip);
            ClearCreateTripsForms();
            InvalidCreateTrip(true);
        }
        private void InvalidCreateTrip(bool t)
        {
            if (t)
            {
                this.label14.Text = "Trip is created";
                this.label14.ForeColor = Color.ForestGreen;
            }
            else
            {
                this.label14.Text = "Not all forms are filled";
                this.label14.ForeColor = Color.Red;
            }
        }
        private void ClearCreateTripsForms()
        {
            this.flowLayoutPanel2.Controls.Clear();
            classObj = new ClassObject(this.flowLayoutPanel2);
            classObj.AddPanel();

            this.AirCompaniesComboBox.SelectedIndex = -1;
            this.CreateTripWhenceTextBox.Text = "";
            this.CreateTripWhereTextBox.Text = "";
            this.CreateTripAircraftTextBox.Text = "";
            this.label14.Text = "";
        }
    }
}
