﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        List<int> tripStatList = new List<int>();
        List<int> classStatList = new List<int>();

        private void button6_Click(object sender, EventArgs e)
        {
            this.dataGridView2.Rows.Clear();

            tripList = Sends.GetTrips(
                this.textBox4.Text,
                this.textBox5.Text,
                this.monthCalendar1.SelectionRange.Start.ToShortDateString(),
                this.checkBox1.Checked,
                this.checkBox2.Checked
            );

            fillDataGridView2();
        }
        
        public void dataGridView2_CellContentClick()
        {
            AirCompani comp = Sends.GetAirCompani(tripList[this.dataGridView2.SelectedRows[0].Index].id_compani + "");
            if(comp != null)
                this.label70.Text = comp.name;

            classList = Sends.GetClasses(tripList[this.dataGridView2.SelectedRows[0].Index].id + "");
            tripStatList = Sends.GetTripStat(tripList[this.dataGridView2.SelectedRows[0].Index].id + "");

            chart1.Series["Series1"].Points.Clear();
            chart2.Series["Series1"].Points.Clear();

            chart1.Series["Series1"].Points.Add(tripStatList[0]);
            chart1.Series["Series1"].Points[0].Color = Color.Tomato;
            chart1.Series["Series1"].Points[0].LegendText = tripStatList[0] + " - Empty";

            chart1.Series["Series1"].Points.Add(tripStatList[1]);
            chart1.Series["Series1"].Points[1].Color = Color.LightBlue;
            chart1.Series["Series1"].Points[1].LegendText = tripStatList[1] + " - Booked";

            chart1.Series["Series1"].Points.Add(tripStatList[2]);
            chart1.Series["Series1"].Points[2].Color = Color.LightGreen;
            chart1.Series["Series1"].Points[2].LegendText = tripStatList[2] + " - Bought";

            fillDataGridView1();
        }
        public void dataGridView1_CellContentClick()
        {
            classStatList = Sends.GetClassStat(classList[this.dataGridView1.SelectedRows[0].Index].id + "");

            chart2.Series["Series1"].Points.Clear();

            chart2.Series["Series1"].Points.Add(classStatList[0]);
            chart2.Series["Series1"].Points[0].Color = Color.Tomato;
            chart2.Series["Series1"].Points[0].LegendText = classStatList[0] + " - Empty";

            chart2.Series["Series1"].Points.Add(classStatList[1]);
            chart2.Series["Series1"].Points[1].Color = Color.LightBlue;
            chart2.Series["Series1"].Points[1].LegendText = classStatList[1] + " - Booked";

            chart2.Series["Series1"].Points.Add(classStatList[2]);
            chart2.Series["Series1"].Points[2].Color = Color.LightGreen;
            chart2.Series["Series1"].Points[2].LegendText = classStatList[2] + " - Bought";
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGridView2.GetCellCount(DataGridViewElementStates.Selected) != 5)
            {
                this.dataGridView2.ClearSelection();
                this.dataGridView1.ClearSelection();
            }
            else
            {
                dataGridView2_CellContentClick();
            }
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGridView1.GetCellCount(DataGridViewElementStates.Selected) != 2)
            {
                this.dataGridView1.ClearSelection();
            }
            else
            {
                dataGridView1_CellContentClick();
            }
        }
        private void fillDataGridView2()
        {
            this.dataGridView2.Rows.Clear();

            if (tripList == null)
                return;

            for (int i = 0; i < tripList.Count; i++)
            {
                DateTime date;
                this.dataGridView2.Rows.Add();

                this.dataGridView2[0, i].Value = tripList[i].fromcity;
                this.dataGridView2[1, i].Value = tripList[i].wherecity;

                date = DateTime.Parse(tripList[i].depature_date);
                this.dataGridView2[2, i].Value = date.ToShortDateString() + "/" + date.ToShortTimeString();

                date = DateTime.Parse(tripList[i].arrival_date);
                this.dataGridView2[3, i].Value = date.ToShortDateString() + "/" + date.ToShortTimeString();

                this.dataGridView2[4, i].Value = tripList[i].aircraft;
            }
        }
        private void fillDataGridView1()
        {
            this.dataGridView1.Rows.Clear();

            for (int i = 0; i < classList.Count; i++)
            {
                this.dataGridView1.Rows.Add();

                this.dataGridView1[0, i].Value = classList[i].name;
                this.dataGridView1[1, i].Value = classList[i].price;
            }
        }

        private void ClearTripsStatisticsPanel()
        {
            this.textBox4.Text = "";
            this.textBox5.Text = "";
            chart1.Series["Series1"].Points.Clear();
            chart2.Series["Series1"].Points.Clear();
            this.dataGridView2.Rows.Clear();
            this.dataGridView1.Rows.Clear();
            this.label70.Text = "";
        }
    }
}
