﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        public MyTrip trip = new MyTrip();

        private void CreateListMytrips()
        {   
            mytripList = Sends.GetMyTrips();

            for (int i = 0; i < mytripList.Count; i++)
            {
                this.MyTripsPanel.Controls.Add(new TripObject(new System.Drawing.Size(431, 100), mytripList[i], this, DefaultPanelMyTrip).getPanel);
            }
          
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (trip.id == 0)
                return;
            if(Sends.BuyBookingTicket(trip.id + ""))
            {
                this.label28.Text = "Ticket is bought";
                this.label28.ForeColor = Color.ForestGreen;
            }
            else
            {
                this.label28.Text = "Ticket is not bought. May be, ticket was bought somebody yet or you did not book it";
                this.label28.ForeColor = Color.Red;
            }
        }
        private void buttonDlete_Click(object sender, EventArgs e)
        {
            if (trip.id == 0)
                return;
            if (Sends.DeleteBooking(trip.id + ""))
            {
                this.label28.Text = "Book is canceled";
                this.label28.ForeColor = Color.ForestGreen;
            }
            else
            {
                this.label28.Text = "Book is could not cancel";
                this.label28.ForeColor = Color.Red;
            }
        }

        public void DefaultPanelMyTrip()
        {
            foreach(Panel obj in this.MyTripsPanel.Controls)
            {
                obj.BackColor = Color.Gainsboro;
            }
        }
        public void FillLabels()
        {
            this.label44.Text = trip.nameCompani;
            this.label20.Text = trip.fromcity;
            this.label21.Text = trip.wherecity;
            this.label22.Text = trip.price;
        }

        private void ClearMyTripsPanel()
        {
            trip = new MyTrip();
            this.MyTripsPanel.Controls.Clear();
            this.label44.Text = "";
            this.label20.Text = "";
            this.label21.Text = "";
            this.label22.Text = "";
            this.label28.Text = "";
        }
    }
}
