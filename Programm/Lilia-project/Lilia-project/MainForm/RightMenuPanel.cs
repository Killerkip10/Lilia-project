﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        int t = 9;

        private void autheticationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutorithationForm authForm = new AutorithationForm();
            authForm.Owner = this;
            authForm.ShowDialog();
        }
        private void tripsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearTripsPanel();
            this.MainPanel.Visible = true;
        }
        private void myTripsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearMyTripsPanel();
            this.MyTripsPanelM.Visible = true;
            CreateListMytrips();
        }
        private void airCompaniesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearAirCompaniPanel();
            this.MyAirCompPanel.Visible = true;
        }
        private void createTripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearCreateTripsForms();

            this.CreateTripPanel.Visible = true;
            myAirCompList = Sends.GetOwnerAirCompani();

            fillMyAircraftComboBox();
        }
        private void label11_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearBookingListPanel();

            this.BookingListPanel.Visible = true;
        }
        private void label65_Click(object sender, EventArgs e)
        {
            this.tabControl1.Visible = !this.tabControl1.Visible;
        }
        private void label67_Click(object sender, EventArgs e)
        {
            CloseAllPanel();
            ClearTripsStatisticsPanel();

            this.TripsStatistic.Visible = true;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            this.RightMenuPanel.Location = new Point(this.Width, 20);
            t = 9;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            t = -t;
            tmForward.Enabled = true;
        }
        private void MenuForward(object obj, EventArgs e)
        {
          
            this.RightMenuPanel.Location = new Point(this.RightMenuPanel.Location.X + t, this.RightMenuPanel.Location.Y);

            if (this.RightMenuPanel.Location.X < this.Width - 260 || this.RightMenuPanel.Location.X > this.Width)
            {
                tmForward.Enabled = false;
            }
        }
        private void label5_MouseHover(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            label.ForeColor = Color.ForestGreen;
        }
        private void label5_MouseLeave(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            label.ForeColor = Color.Black;
        }

        public void WriteUserInfoInRightMenu()
        {
            this.NameSurnameUserLabel.Text = user.name + " " + user.surname;

            this.label56.Text = user.name;
            this.label57.Text = user.surname;
            this.label58.Text = user.email;
            this.label59.Text = user.phone;
            this.label60.Text = user.passport;

            this.label61.Text = owner.name;
            this.label62.Text = owner.surname;
            this.label63.Text = owner.email;
            this.label64.Text = owner.phone;

            this.label55.Text = String.Join("\n", owner.companies.ToArray<String>());

            switch (role)
            {
                case Role.None:
                    {
                        this.RoleUserLabel.Text = "Guest";
                    }
                    break;
                case Role.User:
                    {
                        this.RoleUserLabel.Text = "User";
                    }
                    break;
                case Role.Owner:
                    {
                        this.RoleUserLabel.Text = "Owner";
                    }
                    break;
                case Role.Paymaster:
                    {
                        this.RoleUserLabel.Text = "Paymaster";
                    }
                    break;
                case Role.Manager:
                    {
                        this.RoleUserLabel.Text = "Manager";
                    }
                    break;
                default:
                    {
                        this.RoleUserLabel.Text = "Guest";
                    }
                    break;
            }
        }      
    }
}
