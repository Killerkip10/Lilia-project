﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.models;
using Lilia_project.send;

namespace Lilia_project
{
    public partial class MainForm : Form
    {
        List<MyTrip> usersTrips = new List<MyTrip>();
        User userPay = new User();

        private void button3_Click(object sender, EventArgs e)
        {
            this.flowLayoutPanel3.Controls.Clear();
            this.label31.Text = "";

            if (this.textBox2.Text.Trim() == "")
                return;

            userPay = Sends.GetUser(this.textBox2.Text);

            if (userPay == null || userPay.id == 0)
                return;

            this.label32.Text = userPay.name;
            this.label33.Text = userPay.surname;
            this.label34.Text = userPay.phone;
            this.label35.Text = userPay.email;

            usersTrips = Sends.GetUsersTrips(userPay.id + "");

            if(usersTrips.Count == 0)
            {
                this.label31.Text = "Nothing";
                this.label31.ForeColor = Color.Red;
                return;
            }

            foreach(var obj in usersTrips)
            {
                this.flowLayoutPanel3.Controls.Add(new TripObject(new System.Drawing.Size(431, 100), obj, this, DefaultPanelBookingTrip).getPanel);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (trip.id == 0)
                return;

            if (Sends.BuyUsersTicket(trip.id + ""))
            {
                this.label41.Text = "Ticket is bought";
                this.label41.ForeColor = Color.ForestGreen;
            }
            else
            {
                this.label41.Text = "Ticket is not bought. May be, ticket was bought somebody yet or you did not book it";
                this.label41.ForeColor = Color.Red;
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (trip.id == 0)
                return; 

            if (Sends.DeleteBookingPaymaster(trip.id + ""))
            {
                this.label41.Text = "Book is canceled";
                this.label41.ForeColor = Color.ForestGreen;
            }
            else
            {
                this.label41.Text = "Book is canceled yet";
                this.label41.ForeColor = Color.Red;
            }


        }

        private void ClearBookingListPanel()
        {
            trip = new MyTrip();
            this.textBox2.Text = "";
            this.label31.Text = "";
            this.label32.Text = "";
            this.label33.Text = "";
            this.label34.Text = "";
            this.label35.Text = "";
            this.label41.Text = "";
            this.flowLayoutPanel3.Controls.Clear();
        }
        public void DefaultPanelBookingTrip()
        {
            foreach (Panel obj in this.flowLayoutPanel3.Controls)
            {
                obj.BackColor = Color.Gainsboro;
            }
        }
    }
}
