﻿namespace Lilia_project
{
    partial class AutorithationForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.NoneLoginOrPassword = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.LoginTextBox = new System.Windows.Forms.TextBox();
            this.LoginButton = new System.Windows.Forms.Button();
            this.RegistrationPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.EmailRegLabel = new System.Windows.Forms.Label();
            this.PhoneRegLabel = new System.Windows.Forms.Label();
            this.EmailRegTextBox = new System.Windows.Forms.TextBox();
            this.PhoneRegTextBox = new System.Windows.Forms.TextBox();
            this.SurnameRegLabel = new System.Windows.Forms.Label();
            this.NameRegLabel = new System.Windows.Forms.Label();
            this.Password2RegLabel = new System.Windows.Forms.Label();
            this.Password1RegLabel = new System.Windows.Forms.Label();
            this.LoginRegLabel = new System.Windows.Forms.Label();
            this.SurnameRegTextBox = new System.Windows.Forms.TextBox();
            this.NameRegTextBox = new System.Windows.Forms.TextBox();
            this.Password2RegTextBox = new System.Windows.Forms.TextBox();
            this.Password1RegTextBox = new System.Windows.Forms.TextBox();
            this.LoginRegTextBox = new System.Windows.Forms.TextBox();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.RegistrationLabel = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TabsPanel = new System.Windows.Forms.Panel();
            this.LoginPanel.SuspendLayout();
            this.RegistrationPanel.SuspendLayout();
            this.TabsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoginPanel
            // 
            this.LoginPanel.Controls.Add(this.NoneLoginOrPassword);
            this.LoginPanel.Controls.Add(this.PasswordTextBox);
            this.LoginPanel.Controls.Add(this.LoginTextBox);
            this.LoginPanel.Controls.Add(this.LoginButton);
            this.LoginPanel.Location = new System.Drawing.Point(0, 50);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(383, 270);
            this.LoginPanel.TabIndex = 0;
            // 
            // NoneLoginOrPassword
            // 
            this.NoneLoginOrPassword.AutoSize = true;
            this.NoneLoginOrPassword.ForeColor = System.Drawing.Color.Red;
            this.NoneLoginOrPassword.Location = new System.Drawing.Point(98, 118);
            this.NoneLoginOrPassword.Name = "NoneLoginOrPassword";
            this.NoneLoginOrPassword.Size = new System.Drawing.Size(188, 17);
            this.NoneLoginOrPassword.TabIndex = 5;
            this.NoneLoginOrPassword.Text = "Login or password uncorrect";
            this.NoneLoginOrPassword.Visible = false;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PasswordTextBox.ForeColor = System.Drawing.Color.Gray;
            this.PasswordTextBox.Location = new System.Drawing.Point(29, 77);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(328, 27);
            this.PasswordTextBox.TabIndex = 4;
            this.PasswordTextBox.Text = "Password";
            this.PasswordTextBox.Enter += new System.EventHandler(this.PasswordTextBox_Enter);
            this.PasswordTextBox.Leave += new System.EventHandler(this.PasswordTextBox_Leave);
            // 
            // LoginTextBox
            // 
            this.LoginTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginTextBox.ForeColor = System.Drawing.Color.Gray;
            this.LoginTextBox.Location = new System.Drawing.Point(29, 25);
            this.LoginTextBox.Name = "LoginTextBox";
            this.LoginTextBox.Size = new System.Drawing.Size(328, 27);
            this.LoginTextBox.TabIndex = 3;
            this.LoginTextBox.Text = "Login";
            this.LoginTextBox.Enter += new System.EventHandler(this.LoginTextBox_Enter);
            this.LoginTextBox.Leave += new System.EventHandler(this.LoginTextBox_Leave);
            // 
            // LoginButton
            // 
            this.LoginButton.BackColor = System.Drawing.Color.ForestGreen;
            this.LoginButton.Font = new System.Drawing.Font("Arial Unicode MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginButton.ForeColor = System.Drawing.Color.White;
            this.LoginButton.Location = new System.Drawing.Point(136, 150);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(111, 36);
            this.LoginButton.TabIndex = 2;
            this.LoginButton.Text = "Login";
            this.LoginButton.UseVisualStyleBackColor = false;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // RegistrationPanel
            // 
            this.RegistrationPanel.Controls.Add(this.button1);
            this.RegistrationPanel.Controls.Add(this.EmailRegLabel);
            this.RegistrationPanel.Controls.Add(this.PhoneRegLabel);
            this.RegistrationPanel.Controls.Add(this.EmailRegTextBox);
            this.RegistrationPanel.Controls.Add(this.PhoneRegTextBox);
            this.RegistrationPanel.Controls.Add(this.SurnameRegLabel);
            this.RegistrationPanel.Controls.Add(this.NameRegLabel);
            this.RegistrationPanel.Controls.Add(this.Password2RegLabel);
            this.RegistrationPanel.Controls.Add(this.Password1RegLabel);
            this.RegistrationPanel.Controls.Add(this.LoginRegLabel);
            this.RegistrationPanel.Controls.Add(this.SurnameRegTextBox);
            this.RegistrationPanel.Controls.Add(this.NameRegTextBox);
            this.RegistrationPanel.Controls.Add(this.Password2RegTextBox);
            this.RegistrationPanel.Controls.Add(this.Password1RegTextBox);
            this.RegistrationPanel.Controls.Add(this.LoginRegTextBox);
            this.RegistrationPanel.Location = new System.Drawing.Point(3, 3);
            this.RegistrationPanel.Name = "RegistrationPanel";
            this.RegistrationPanel.Size = new System.Drawing.Size(386, 274);
            this.RegistrationPanel.TabIndex = 3;
            this.RegistrationPanel.Visible = false;
            this.RegistrationPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.RegistrationPanel_Paint);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.RoyalBlue;
            this.button1.Font = new System.Drawing.Font("Arial Unicode MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(224, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 34);
            this.button1.TabIndex = 18;
            this.button1.Text = "Sign up";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // EmailRegLabel
            // 
            this.EmailRegLabel.AutoSize = true;
            this.EmailRegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EmailRegLabel.Location = new System.Drawing.Point(12, 208);
            this.EmailRegLabel.Name = "EmailRegLabel";
            this.EmailRegLabel.Size = new System.Drawing.Size(51, 20);
            this.EmailRegLabel.TabIndex = 17;
            this.EmailRegLabel.Text = "Email";
            // 
            // PhoneRegLabel
            // 
            this.PhoneRegLabel.AutoSize = true;
            this.PhoneRegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhoneRegLabel.Location = new System.Drawing.Point(12, 174);
            this.PhoneRegLabel.Name = "PhoneRegLabel";
            this.PhoneRegLabel.Size = new System.Drawing.Size(62, 20);
            this.PhoneRegLabel.TabIndex = 16;
            this.PhoneRegLabel.Text = "Phone*";
            // 
            // EmailRegTextBox
            // 
            this.EmailRegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EmailRegTextBox.ForeColor = System.Drawing.Color.Black;
            this.EmailRegTextBox.Location = new System.Drawing.Point(111, 205);
            this.EmailRegTextBox.Name = "EmailRegTextBox";
            this.EmailRegTextBox.Size = new System.Drawing.Size(256, 27);
            this.EmailRegTextBox.TabIndex = 15;
            // 
            // PhoneRegTextBox
            // 
            this.PhoneRegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhoneRegTextBox.ForeColor = System.Drawing.Color.Black;
            this.PhoneRegTextBox.Location = new System.Drawing.Point(111, 171);
            this.PhoneRegTextBox.Name = "PhoneRegTextBox";
            this.PhoneRegTextBox.Size = new System.Drawing.Size(256, 27);
            this.PhoneRegTextBox.TabIndex = 14;
            // 
            // SurnameRegLabel
            // 
            this.SurnameRegLabel.AutoSize = true;
            this.SurnameRegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SurnameRegLabel.Location = new System.Drawing.Point(12, 140);
            this.SurnameRegLabel.Name = "SurnameRegLabel";
            this.SurnameRegLabel.Size = new System.Drawing.Size(82, 20);
            this.SurnameRegLabel.TabIndex = 13;
            this.SurnameRegLabel.Text = "Surname*";
            // 
            // NameRegLabel
            // 
            this.NameRegLabel.AutoSize = true;
            this.NameRegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameRegLabel.Location = new System.Drawing.Point(12, 106);
            this.NameRegLabel.Name = "NameRegLabel";
            this.NameRegLabel.Size = new System.Drawing.Size(59, 20);
            this.NameRegLabel.TabIndex = 12;
            this.NameRegLabel.Text = "Name*";
            this.NameRegLabel.Click += new System.EventHandler(this.NameRegLabel_Click);
            // 
            // Password2RegLabel
            // 
            this.Password2RegLabel.AutoSize = true;
            this.Password2RegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Password2RegLabel.Location = new System.Drawing.Point(12, 72);
            this.Password2RegLabel.Name = "Password2RegLabel";
            this.Password2RegLabel.Size = new System.Drawing.Size(68, 20);
            this.Password2RegLabel.TabIndex = 11;
            this.Password2RegLabel.Text = "Repeat*";
            // 
            // Password1RegLabel
            // 
            this.Password1RegLabel.AutoSize = true;
            this.Password1RegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Password1RegLabel.Location = new System.Drawing.Point(12, 37);
            this.Password1RegLabel.Name = "Password1RegLabel";
            this.Password1RegLabel.Size = new System.Drawing.Size(89, 20);
            this.Password1RegLabel.TabIndex = 10;
            this.Password1RegLabel.Text = "Password*";
            // 
            // LoginRegLabel
            // 
            this.LoginRegLabel.AutoSize = true;
            this.LoginRegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginRegLabel.Location = new System.Drawing.Point(12, 5);
            this.LoginRegLabel.Name = "LoginRegLabel";
            this.LoginRegLabel.Size = new System.Drawing.Size(56, 20);
            this.LoginRegLabel.TabIndex = 9;
            this.LoginRegLabel.Text = "Login*";
            // 
            // SurnameRegTextBox
            // 
            this.SurnameRegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SurnameRegTextBox.ForeColor = System.Drawing.Color.Black;
            this.SurnameRegTextBox.Location = new System.Drawing.Point(111, 137);
            this.SurnameRegTextBox.Name = "SurnameRegTextBox";
            this.SurnameRegTextBox.Size = new System.Drawing.Size(256, 27);
            this.SurnameRegTextBox.TabIndex = 8;
            // 
            // NameRegTextBox
            // 
            this.NameRegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameRegTextBox.ForeColor = System.Drawing.Color.Black;
            this.NameRegTextBox.Location = new System.Drawing.Point(111, 103);
            this.NameRegTextBox.Name = "NameRegTextBox";
            this.NameRegTextBox.Size = new System.Drawing.Size(256, 27);
            this.NameRegTextBox.TabIndex = 7;
            // 
            // Password2RegTextBox
            // 
            this.Password2RegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Password2RegTextBox.ForeColor = System.Drawing.Color.Black;
            this.Password2RegTextBox.Location = new System.Drawing.Point(111, 69);
            this.Password2RegTextBox.Name = "Password2RegTextBox";
            this.Password2RegTextBox.Size = new System.Drawing.Size(256, 27);
            this.Password2RegTextBox.TabIndex = 6;
            // 
            // Password1RegTextBox
            // 
            this.Password1RegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Password1RegTextBox.ForeColor = System.Drawing.Color.Black;
            this.Password1RegTextBox.Location = new System.Drawing.Point(111, 34);
            this.Password1RegTextBox.Name = "Password1RegTextBox";
            this.Password1RegTextBox.Size = new System.Drawing.Size(256, 27);
            this.Password1RegTextBox.TabIndex = 5;
            // 
            // LoginRegTextBox
            // 
            this.LoginRegTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginRegTextBox.ForeColor = System.Drawing.Color.Black;
            this.LoginRegTextBox.Location = new System.Drawing.Point(112, 0);
            this.LoginRegTextBox.Name = "LoginRegTextBox";
            this.LoginRegTextBox.Size = new System.Drawing.Size(256, 27);
            this.LoginRegTextBox.TabIndex = 4;
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Font = new System.Drawing.Font("Arial Unicode MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.LoginLabel.Location = new System.Drawing.Point(13, 13);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(49, 23);
            this.LoginLabel.TabIndex = 1;
            this.LoginLabel.Text = "Login";
            this.LoginLabel.Click += new System.EventHandler(this.LoginLabel_Click);
            // 
            // RegistrationLabel
            // 
            this.RegistrationLabel.AutoSize = true;
            this.RegistrationLabel.Font = new System.Drawing.Font("Arial Unicode MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegistrationLabel.Location = new System.Drawing.Point(80, 13);
            this.RegistrationLabel.Name = "RegistrationLabel";
            this.RegistrationLabel.Size = new System.Drawing.Size(98, 23);
            this.RegistrationLabel.TabIndex = 2;
            this.RegistrationLabel.Text = "Registration";
            this.RegistrationLabel.Click += new System.EventHandler(this.RegistrationLabel_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // TabsPanel
            // 
            this.TabsPanel.Controls.Add(this.RegistrationPanel);
            this.TabsPanel.Location = new System.Drawing.Point(-3, 50);
            this.TabsPanel.Name = "TabsPanel";
            this.TabsPanel.Size = new System.Drawing.Size(382, 276);
            this.TabsPanel.TabIndex = 3;
            // 
            // AutorithationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 323);
            this.Controls.Add(this.LoginPanel);
            this.Controls.Add(this.TabsPanel);
            this.Controls.Add(this.RegistrationLabel);
            this.Controls.Add(this.LoginLabel);
            this.MaximumSize = new System.Drawing.Size(399, 370);
            this.MinimumSize = new System.Drawing.Size(399, 370);
            this.Name = "AutorithationForm";
            this.Text = "Form1";
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.RegistrationPanel.ResumeLayout(false);
            this.RegistrationPanel.PerformLayout();
            this.TabsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Label RegistrationLabel;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox LoginTextBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel RegistrationPanel;
        private System.Windows.Forms.TextBox SurnameRegTextBox;
        private System.Windows.Forms.TextBox NameRegTextBox;
        private System.Windows.Forms.TextBox Password2RegTextBox;
        private System.Windows.Forms.TextBox Password1RegTextBox;
        private System.Windows.Forms.TextBox LoginRegTextBox;
        private System.Windows.Forms.Panel TabsPanel;
        private System.Windows.Forms.Label EmailRegLabel;
        private System.Windows.Forms.Label PhoneRegLabel;
        private System.Windows.Forms.TextBox EmailRegTextBox;
        private System.Windows.Forms.TextBox PhoneRegTextBox;
        private System.Windows.Forms.Label SurnameRegLabel;
        private System.Windows.Forms.Label NameRegLabel;
        private System.Windows.Forms.Label Password2RegLabel;
        private System.Windows.Forms.Label Password1RegLabel;
        private System.Windows.Forms.Label LoginRegLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label NoneLoginOrPassword;
    }
}

