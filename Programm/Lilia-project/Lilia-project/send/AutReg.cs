﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;

using Lilia_project.models;
using Lilia_project.cookies;
using System.Web;
using System.Security.Claims;

namespace Lilia_project.send
{
    class AutReg
    {
        public static Role Autorization(UserModel user, MainForm mainform)
        {
            try
            {
                HttpWebResponse res = Request.Post(user, "/auth/login");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                if (line.Trim() == "")
                {
                    mainform.user = new User();
                }
                else
                {
                    mainform.user = Request.ToObj<User>(line);
                }

                Owner owner = Sends.GetOwner();

                if (owner == null)
                {
                    mainform.owner = new Owner();
                }
                else
                {
                    mainform.owner = owner;
                }

                if (res.Cookies["token"] == null)
                    return Role.None;

                var handler = new JwtSecurityTokenHandler();
                var claims = handler.ReadJwtToken(res.Cookies["token"].Value).Claims;
                
                    foreach (Claim c in claims)
                    {
                        if (c.Type == "type")
                        {
                            switch (c.Value)
                            {
                                case "1":
                                    {
                                        return Role.User;
                                    }
                                    break;
                                case "2":
                                    {
                                        return Role.Owner;
                                    }
                                    break;
                                case "3":
                                    {
                                        return Role.Manager;
                                    }
                                case "4":
                                    {
                                        return Role.Paymaster;
                                    }
                                default:
                                    {
                                        return Role.None;
                                    }
                            }
                            break;
                        }
                    }
                return Role.None;
            }
            catch (Exception err)
            {
                return Role.None;
            }
        }
    }
}
