﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Lilia_project.models;

namespace Lilia_project.send
{
    abstract class Sends
    {
        public static List<Trip> GetTrips(String whence, String where, String date, bool exactDate, bool allTime)
        {
            Uri url = new Uri(Config.urlHost + Config.urlPort + "/data/trip/");

            if (whence.Trim() != "")
                url = AddQuery(url, "whencecity", whence);
            if(where.Trim() != "")
                url = AddQuery(url, "wherecity", where);

            url = AddQuery(url, "date", date);
            url = AddQuery(url, "exact", Convert.ToString(Convert.ToInt16(exactDate)));
            url = AddQuery(url, "alltime", Convert.ToString(Convert.ToInt16(allTime)));
            String u = url.PathAndQuery;

            try
            {
                HttpWebResponse res = Request.Get(u);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<Trip>)Request.ToObj<List<Trip>>(line);
            }catch(WebException e)
            {
                return new List<Trip>();
            }
        } 
        public static AirCompani GetAirCompani(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/aircompani/get/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (AirCompani)Request.ToObj<AirCompani>(line);
            }
            catch (WebException e)
            {
                return new AirCompani();
            }
        }
        public static List<Class> GetClasses(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/aircompani/class/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<Class>)Request.ToObj<List<Class>>(line);
            }catch(WebException e)
            {
                return new List<Class>();
            }
        }
        public static List<Siting> GetSits(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/aircompani/sits/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<Siting>)Request.ToObj<List<Siting>>(line);
            }catch(WebException e)
            {
                return new List<Siting>();
            }
        }
        public static int BookSits(SitingSend list)
        {
            try
            {
                HttpWebResponse res = Request.Post(list, "/data/aircompani/booksits");

                return 1;
            }
            catch (WebException e)
            {
                return 2;
            }
        }
        public static List<MyTrip> GetMyTrips()
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/my");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                
                return (List<MyTrip>)Request.ToObj<List<MyTrip>>(line);
            }
            catch (WebException e)
            {
                return new List<MyTrip>();
            }
        }
        public static List<MyTrip> GetUsersTrips(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/user/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<MyTrip>)Request.ToObj<List<MyTrip>>(line);
            }catch(WebException e)
            {
                return new List<MyTrip>();
            }
        }
        public static bool DeleteBooking(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/delete/" + id);
                
                return true;
            }catch(WebException e)
            {
                return false;
            }
        }
        public static bool DeleteBookingPaymaster(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/deletepay/" + id);

                return true;
            }
            catch (WebException e)
            {
                return false;
            }
        }
        public static bool BuyBookingTicket(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/buybooking/" + id);

                return true;
            }catch(WebException e)
            {
                return false;
            }
        }
        public static bool BuyUsersTicket(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/trip/buy/" + id);

                return true;
            }catch(WebException e)
            {
                return false;
            }
        }
        public static bool BuyTickets(SitingSend list)
        {
            try
            {
                HttpWebResponse res = Request.Post(list, "/data/trip/buy");

                return true;
            }
            catch (WebException e)
            {
                return false;
            }
        }
        public static List<AirCompani> GetOwnerAirCompani()
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/aircompani/owner");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<AirCompani>)Request.ToObj<List<AirCompani>>(line);
            }
            catch (WebException e)
            {
                return new List<AirCompani>();
            }
        }
        public static User GetUser(String passport)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/account/passport/" + passport);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                String line = reader.ReadToEnd();

                return (User)Request.ToObj<User>(line);
            }catch(WebException e)
            {
                return new User();
            }
        }
        public static Owner GetOwner()
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/account/owner");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (Owner)Request.ToObj<Owner>(line);
            }catch(WebException e)
            {
                return new Owner();
            }
        }
        public static List<int> GetTripStat(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/statistics/trip/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<int>)Request.ToObj<List<int>>(line);

            }catch(WebException e)
            {
                return new List<int>();
            }
        }
        public static List<int> GetClassStat(String id)
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/statistics/class/" + id);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<int>)Request.ToObj<List<int>>(line);
            }
            catch (WebException e)
            {
                return new List<int>();
            }
        }
        public static List<int> GetAirCompaniStat(String id, int month)
        {
            Uri url = new Uri(Config.urlHost + Config.urlPort + "/data/statistics/aircompani/");

            url = AddQuery(url, "id", id);
            url = AddQuery(url, "month", month + "");
            String u = url.PathAndQuery;

            try
            {
                HttpWebResponse res = Request.Get(u);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<int>)Request.ToObj<List<int>>(line);
            }catch(WebException e)
            {
                return new List<int>();
            }
        }
        public static List<User> GetStaff()
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/account/staff");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return Request.ToObj<List<User>>(line);
            }catch(WebException e)
            {
                return new List<User>();
            }
        }
        public static List<int> GetStaffStat(String id, int month)
        {
            Uri url = new Uri(Config.urlHost + Config.urlPort + "/data/statistics/staff/");

            url = AddQuery(url, "id", id);
            url = AddQuery(url, "month", month + "");
            String u = url.PathAndQuery;

            try
            {
                HttpWebResponse res = Request.Get(u);
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<int>)Request.ToObj<List<int>>(line);
            }
            catch (WebException e)
            {
                return new List<int>();
            }
        }

        //admins
        public static bool CreateAirCompani(CreateAirCompSend obj)
        {
            try
            {
                Request.Post(obj, "/data/aircompani/create");

                return true;
            }catch(WebException e)
            {
                return false;
            }
        }
        public static List<AirCompani> GetMyAirCompani()
        {
            try
            {
                HttpWebResponse res = Request.Get("/data/aircompani/my");
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadToEnd();

                return (List<AirCompani>)Request.ToObj<List<AirCompani>>(line);
            }catch(WebException e)
            {
                return new List<AirCompani>();
            }
        }
        public static bool CreateTrip(CreateTripSend obj)
        {
            try
            {
                Request.Post(obj, "/data/trip/create");

                return true;
            }catch(WebException e)
            {
                return false;
            }
        }
        //
        private static Uri AddQuery(Uri uri, string name, string value)
        {
            var httpValueCollection = HttpUtility.ParseQueryString(uri.Query);

            httpValueCollection.Remove(name);
            httpValueCollection.Add(name, value);

            var ub = new UriBuilder(uri);
            ub.Query = httpValueCollection.ToString();

            return ub.Uri;
        }
    }
}
