﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Lilia_project.models;
using Lilia_project.cookies;

namespace Lilia_project.send
{
    abstract class Request
    {
        static public CookieContainer cookies = new CookieContainer();

        static public HttpWebResponse Post (object obj, string path)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Config.urlHost + Config.urlPort + path);
            req.Method = "POST";
            req.ContentType = "application/json";
            req.CookieContainer = cookies;

           byte[] byteArray = Encoding.UTF8.GetBytes(ToJson(obj));

            Stream dataStream = req.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();     
   
            return res;
        }
        static public HttpWebResponse Get(string path)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Config.urlHost + Config.urlPort + path);
            req.CookieContainer = cookies;
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            
            return res;
        }
        static public string ToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj); ;
        }
        static public T ToObj<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        } 
    }
}
