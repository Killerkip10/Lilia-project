﻿using Lilia_project.models;
using Lilia_project.send;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lilia_project
{
    delegate void CallBack();

    class TripObject
    {
        Panel panel = new Panel();
        MyTrip trip;
        MainForm mainform;
        CallBack del;

        Font font1 = new Font("Microsoft Sans Serif", 15.8F, GraphicsUnit.Pixel);
        Font font2 = new Font("Microsoft Sans Serif", 12.8F, GraphicsUnit.Pixel);
        Font font3 = new Font("Microsoft Sans Serif", 13.8F, GraphicsUnit.Pixel);

        public TripObject(System.Drawing.Size size, MyTrip trip, MainForm mainform, CallBack del)
        {
            this.trip = trip;
            this.del = del;
            this.mainform = mainform;

            DateTime date1, date2;
            TimeSpan date3;

            panel.Name = "";
            panel.Size = size;
            panel.BackColor = Color.LightGray;

            panel.Controls.Add(getPanelInside(new Size(5, 100), new Point(0, 0), trip.paid ? Color.ForestGreen : Color.Red));
         
            panel.Controls.Add(getLabel(trip.fromcity, 10, 10, new Size(160, 20), Color.Black, font1, ContentAlignment.BottomCenter));

            panel.Controls.Add(getPictureBox("D:\\Лабораторки\\Курсачи\\Курсач_C#_2_курс_3_сем\\Programm\\Lilia-project\\Lilia-project\\picture\\arrow.png", new Point(185, 5), new Size(50, 30), false));

            panel.Controls.Add(getLabel(trip.wherecity, 240, 10, new Size(160, 20), Color.Black, font1, ContentAlignment.BottomCenter));

            panel.Controls.Add(getLabel("Class : ", 10, 50, new Size(50, 20), Color.DimGray, font2, ContentAlignment.TopLeft));
            panel.Controls.Add(getLabel(trip.name, 60, 50, new Size(100, 20), Color.DimGray, font2, ContentAlignment.TopLeft));

            panel.Controls.Add(getLabel("Sit : ", 185, 50, new Size(35, 20), Color.DimGray, font2, ContentAlignment.TopLeft));
            panel.Controls.Add(getLabel(trip.number, 220, 50, new Size(70, 20), Color.DimGray, font2, ContentAlignment.TopLeft));

            panel.Controls.Add(getLabel("Price : ", 300, 50, new Size(50, 20), Color.DimGray, font2, ContentAlignment.TopLeft));
            panel.Controls.Add(getLabel(trip.price, 350, 50, new Size(40, 20), Color.Black, font3, ContentAlignment.TopRight));

            panel.Controls.Add(getPanelInside(new Size(431, 1), new Point(0, 75), Color.Black));
            panel.Controls.Add(getPanelInside(new Size(1, 25), new Point(142, 75), Color.Black));
            panel.Controls.Add(getPanelInside(new Size(1, 25), new Point(284, 75), Color.Black));

            date1 = DateTime.Parse(trip.depature_date);
            date2 = DateTime.Parse(trip.arrival_date);
            date3 = date2.Subtract(date1);

            panel.Controls.Add(getLabel(date1.ToShortDateString() + " " + date1.ToShortTimeString(), 10, 80, new Size(140, 20), Color.DimGray, font2, ContentAlignment.TopCenter));
 
            panel.Controls.Add(getLabel(date3.ToString(), 144, 80, new Size(140, 20), Color.ForestGreen, font2, ContentAlignment.TopCenter));

            panel.Controls.Add(getLabel(date2.ToShortDateString() + " " + date2.ToShortTimeString(), 286, 80, new Size(135, 20), Color.DimGray, font2, ContentAlignment.TopCenter));

            //panel.Controls.Add(getLabel("Where : ", 225, 10, new Size(60, 20), Color.Black));
            //panel.Controls.Add(getLabel(trip.wherecity, 285, 10, new Size(150, 20), Color.Black));

            //DateTime date;
            //panel.Controls.Add(getLabel("Depature date : ", 10, 40, new Size(80, 20), Color.Black));
            //date = DateTime.Parse(trip.depature_date);
            //panel.Controls.Add(getLabel(date.ToShortDateString() + "/" + date.ToShortTimeString(), 95, 40, new Size(130, 20), Color.Black));

            //panel.Controls.Add(getLabel("Arrival date : ", 225, 40, new Size(80, 20), Color.Black));
            //date = DateTime.Parse(trip.arrival_date);
            //panel.Controls.Add(getLabel(date.ToShortDateString() + "/" + date.ToShortTimeString(), 305, 40, new Size(130, 20), Color.Black));

            //panel.Controls.Add(getLabel("Class : ", 10, 70, new Size(60, 20), Color.Black));
            //panel.Controls.Add(getLabel(trip.name, 75, 70, new Size(145, 20), Color.Black));

            //panel.Controls.Add(getLabel("Sit number : ", 225, 70, new Size(80, 20), Color.Black));
            //panel.Controls.Add(getLabel(trip.number, 305, 70, new Size(30, 20), Color.Black));

            //panel.Controls.Add(getLabel("Price : ", 340, 70, new Size(60, 20), Color.Black));
            //panel.Controls.Add(getLabel(trip.price, 400, 70, new Size(60, 20), Color.Black));

            panel.Click += new EventHandler(ClickPanel);
        }
        public Panel getPanel
        {
            get { return panel; }
        }

        private void ClickPanel(object o, EventArgs e)
        {

            del();
            panel.BackColor = Color.LightSteelBlue;
            mainform.trip = trip;
            mainform.FillLabels();
        }

        public Panel getPanelInside(Size size, Point point, Color color)
        {
            Panel panel = new Panel();

            panel.Size = size;
            panel.Location = point;
            panel.BackColor = color;
            panel.Click += new EventHandler(ClickPanel);

            return panel;
        }
        private Label getLabel(String text, int x, int y, Size size, Color color, Font font, ContentAlignment type)
        {
            Label label = new Label();

            label.Text = text;
            label.Location = new System.Drawing.Point(x, y);
            label.Size = size;
            label.ForeColor = color;
            label.Font = font;
            label.TextAlign = type;
            label.Click += new EventHandler(ClickPanel); 

            return label;
        }
        private Button getButton(String text, Color color, Point point)
        {
            Button but = new Button();
            but.Text = text;
            but.Location = point;
            but.BackColor = color;
            but.FlatStyle = FlatStyle.Popup;
            but.Click += new EventHandler(ClickPanel);

            return but;
        }
        private PictureBox getPictureBox(String path, Point point, Size size, bool t)
        {
            PictureBox pic = new PictureBox();

            pic.Image = Image.FromFile(path);
            pic.SizeMode = PictureBoxSizeMode.StretchImage;
            pic.Size = size;
            pic.Location = point;
            pic.Cursor = Cursors.Hand;
            pic.Click += new EventHandler(ClickPanel);

            return pic;
        }  
    }
}
