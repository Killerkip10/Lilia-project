﻿using Lilia_project.models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lilia_project
{
    class ClassObject
    {
        Panel parentPanel;
        public List<TextBox> textboxList = new List<TextBox>(12);//Убрать если не нужен

        public ClassObject(FlowLayoutPanel obj)
        {
            parentPanel = obj;
        }
        
        public void AddPanel()
        {
            parentPanel.Controls.Add(CreatePanel());
        }
        private Panel CreatePanel()
        {
            Panel panel = new Panel();

            panel.Size = new Size(550, 40);
            panel.BackColor = Color.Gainsboro;

            panel.Controls.Add(GetLabel("Name : ", new Point(10, 11), new Size(60, 20)));
            panel.Controls.Add(GetTextBox(new Point(70, 9), new Size(150, 20)));

            panel.Controls.Add(GetLabel("Price : ", new Point(240, 11), new Size(50, 20)));
            panel.Controls.Add(GetTextBox(new Point(290, 9), new Size(80, 20)));

            panel.Controls.Add(GetLabel("Sits : ", new Point(390, 11), new Size(45, 20)));
            panel.Controls.Add(GetTextBox(new Point(435, 9), new Size(80, 20)));

            return panel;
        }
        private TextBox GetTextBox(Point point, Size size)
        {
            TextBox textBox = new TextBox();

            textBox.Font = new Font("Microsoft Sans Serif", 13.8F, GraphicsUnit.Pixel);
            textBox.Location = point;
            textBox.Size = size;
            textBox.BackColor = Color.Honeydew;
            textBox.ForeColor = Color.RoyalBlue;

            textboxList.Add(textBox);

            return textBox;
        }
        private Label GetLabel(String text, Point point, Size size)
        {
            Label label = new Label();

            label.Text = text;
            label.Location = point;
            label.Size = size;
            label.Font = new Font("Microsoft Sans Serif", 13.8F, GraphicsUnit.Pixel);

            return label;
        }
        
        public bool CheckValid()
        {
            for(int i = 0; i < textboxList.Count; i++)
            {
                if (textboxList[i].Text.Trim() == "")
                    return false;
            }
            for(int i = 1; i < textboxList.Count; i += 3)
            {
             
                try
                {
                    Convert.ToSingle(textboxList[i].Text);
                    Convert.ToInt32(textboxList[i + 1].Text);
                }catch(Exception e)
                {
                    return false;
                }
            }

            return true;
        }
        public List<Class> GetClassList()
        {
            List<Class> classList = new List<Class>(10);

            for(int i = 0; i < textboxList.Count; i += 3)
            {
                classList.Add(new Class(textboxList[i].Text, textboxList[i + 1].Text, textboxList[i + 2].Text));
            }

            return classList;
        }
        public void ClearForms()
        {
            for(int i = 0; i < textboxList.Count; i++)
            {
                textboxList[i].Text = "";
            }
        }
    }
}
