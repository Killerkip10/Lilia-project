﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lilia_project.send;
using Lilia_project.models;

namespace Lilia_project
{
    public partial class AutorithationForm : Form
    {
        bool LoginPlaceHold = true;
        bool PasswordPlaceHold = true;

        public AutorithationForm()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (LoginTextBox.Text.Trim() != "" && PasswordTextBox.Text.Trim() != "" 
                && LoginPlaceHold == false && PasswordPlaceHold == false)
            {
                MainForm main = (MainForm)this.Owner;

                switch (AutReg.Autorization(new UserModel(LoginTextBox.Text, PasswordTextBox.Text), (MainForm)this.Owner))
                {
                    case Role.None:
                    {
                        NoneLoginOrPassword.Visible = true;
                        main.role = Role.None;
                        main.DefaultStyle();
                        main.Guest();
                    }break;
                    case Role.User:
                    {
                        this.Close();
                        main.role = Role.User;
                        main.DefaultStyle();
                        main.UserStyle();
                    }
                    break;
                    case Role.Owner:
                    {
                        this.Close();
                        main.role = Role.Owner;
                        main.DefaultStyle();
                        main.AdminStyle();
                    }
                    break;
                    case Role.Manager:
                    {
                        this.Close();
                        main.role = Role.Manager;
                        main.DefaultStyle();
                        main.ManagerStyle();
                    }break;
                    case Role.Paymaster:
                    {
                        this.Close();
                        main.role = Role.Paymaster;
                        main.DefaultStyle();
                        main.PayMasterStyle();
                    }break;
                }
                main.WriteUserInfoInRightMenu();
            }
        }
        
        private void LoginTextBox_Enter(object sender, EventArgs e)
        {
            if (LoginPlaceHold)
            {
                LoginTextBox.Text = "";
                LoginPlaceHold = false;
                NonPlaceHoldStyle(LoginTextBox);
            }
        }
        private void LoginTextBox_Leave(object sender, EventArgs e)
        {
            if (LoginTextBox.Text.Trim() == "")
            {
                LoginTextBox.Text = "Login";
                LoginPlaceHold = true;
                PlaceHoldStyle(LoginTextBox);
            }
        }
        private void PasswordTextBox_Enter(object sender, EventArgs e)
        {
            if (PasswordPlaceHold)
            {
                PasswordTextBox.Text = "";
                PasswordPlaceHold = false;
                NonPlaceHoldStyle(PasswordTextBox);
                PasswordTextBox.PasswordChar = '*';
            }
        }
        private void PasswordTextBox_Leave(object sender, EventArgs e)
        {
            if (PasswordTextBox.Text.Trim() == "")
            {
                PasswordTextBox.Text = "Password";
                PasswordPlaceHold = true;
                PlaceHoldStyle(PasswordTextBox);
                PasswordTextBox.PasswordChar = '\0';
            }
        }
        private void PlaceHoldStyle(TextBox obj)
        {
            obj.ForeColor = Color.Gray;
        }
        private void NonPlaceHoldStyle(TextBox obj)
        {
            obj.ForeColor = Color.Black;
        }

        private void LoginLabel_Click(object sender, EventArgs e)
        {
            LoginPanel.Visible = true;
            LoginLabel.ForeColor = Color.ForestGreen;
            RegistrationLabel.ForeColor = Color.Black;
            RegistrationPanel.Visible = false;
        }
        private void RegistrationLabel_Click(object sender, EventArgs e)
        {
            LoginPanel.Visible = false;
            LoginLabel.ForeColor = Color.Black;
            RegistrationLabel.ForeColor = Color.RoyalBlue;
            RegistrationPanel.Visible = true;    
        }
       
        
        
        
        //DELETE НАХЕР
        private void RegistrationPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void NameRegLabel_Click(object sender, EventArgs e)
        {

        }

        
    }
}
