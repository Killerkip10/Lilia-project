﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    class CreateAirCompSend
    {
        public string name = "";
        public string description = "";

        public CreateAirCompSend(string name, string description)
        {
            this.name = name;
            this.description = description;
        }
    }
}
