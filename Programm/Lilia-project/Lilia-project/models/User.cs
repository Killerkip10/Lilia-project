﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    public class User
    {
        public int id;
        public string login = "";
        public string password = "";
        public string name = "";
        public string surname = "";
        public string phone = "";
        public string email = "";
        public string passport = "";
        public string role = "";

        public User()
        {

        }
        public User(int id, string login, string password, string name, string surname, string phone, string email, string passport, string role)
        {
            this.id = id;
            this.login = login;
            this.password = password;
            this.name = name;
            this.surname = surname;
            this.phone = phone;
            this.email = email;
            this.passport = passport;
            this.role = role;
        }
    }
}
