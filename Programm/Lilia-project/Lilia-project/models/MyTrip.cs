﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    public class MyTrip
    {
        public int id;
        public bool paid;
        public String number;
        public String name;
        public String price;
        public String depature_date;
        public String arrival_date;
        public String fromcity;
        public String wherecity;
        public String nameCompani;
    }
}
