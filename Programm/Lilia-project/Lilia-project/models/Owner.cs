﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    public class Owner
    {
        public int id;
        public string name = "";
        public string surname = "";
        public string phone = "";
        public string email = "";
        public List<String> companies = new List<String>();

        public Owner()
        {

        }
        public Owner(int id, string name, string surname, string phone, string email, List<String> companies)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.phone = phone;
            this.email = email;
            this.companies = companies;
        }
    }
}
