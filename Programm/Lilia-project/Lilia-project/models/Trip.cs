﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    class Trip
    {
        public int id;
        public int id_compani;
        public string fromcity = "";
        public string wherecity = "";
        public string depature_date = "";
        public string arrival_date = "";
        public string aircraft = "";

        public Trip(int id_compani, string fromcity, string wherecity, string depature_date, string arrival_date, string aircraft)
        {
            this.id_compani = id_compani;
            this.fromcity = fromcity;
            this.wherecity = wherecity;
            this.depature_date = depature_date;
            this.arrival_date = arrival_date;
            this.aircraft = aircraft;
        }
    }
}
