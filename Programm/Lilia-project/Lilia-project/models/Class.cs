﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    class Class
    {
        public int id;
        public int id_trip;
        public string name = "";
        public float price;
        public int siats;

        public Class()
        {

        }
        public Class(string name, string price, string siats)
        {
            this.name = name;
            this.price = Convert.ToSingle(price);
            this.siats = Convert.ToInt32(siats);
        }
    }
}
