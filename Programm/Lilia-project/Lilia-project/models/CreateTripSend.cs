﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project.models
{
    class CreateTripSend
    {
        public Trip trip;
        public List<Class> classes;

        public CreateTripSend(Trip trip, List<Class> airComp)
        {
            this.trip = trip;
            this.classes = airComp;
        }
    }
}
