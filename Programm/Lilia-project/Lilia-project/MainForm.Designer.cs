﻿using System.Windows.Forms;

namespace Lilia_project
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.CreateTripPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.ArrivalDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.DepatureDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.CreateTripButton = new System.Windows.Forms.Button();
            this.AirCompaniesComboBox = new System.Windows.Forms.ComboBox();
            this.CreateTripAddClassButton = new System.Windows.Forms.Button();
            this.CreateTripWhenceTextBox = new System.Windows.Forms.TextBox();
            this.CreateTripWhereTextBox = new System.Windows.Forms.TextBox();
            this.CreateTripWhenceLabel = new System.Windows.Forms.Label();
            this.CreateTripWhereLabel = new System.Windows.Forms.Label();
            this.CreateTripAircraftTextBox = new System.Windows.Forms.TextBox();
            this.CrateTripAircraftLabel = new System.Windows.Forms.Label();
            this.MyAirCompPanel = new System.Windows.Forms.Panel();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.button7 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.DescriptionAirCompTextBox = new System.Windows.Forms.TextBox();
            this.CreateAirCompButton = new System.Windows.Forms.Button();
            this.NameAirCompLabel = new System.Windows.Forms.Label();
            this.GetMyAirCompButton = new System.Windows.Forms.Button();
            this.LogoCreateAirCompLabel = new System.Windows.Forms.Label();
            this.DescriptionAirCompLabel = new System.Windows.Forms.Label();
            this.NameAirCompTextBox = new System.Windows.Forms.TextBox();
            this.NameAirCompaniLabel = new System.Windows.Forms.Label();
            this.MyAirCompDataGridView = new System.Windows.Forms.DataGridView();
            this.NameAirComp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionAirComp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.BuyTicketMainPanelButton = new System.Windows.Forms.Button();
            this.DescrAirCompaniMainPanelTextBox = new System.Windows.Forms.Label();
            this.NameAirCompaniMainPanelTextBox = new System.Windows.Forms.Label();
            this.BookingResultLabel1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.WhenceTextBox = new System.Windows.Forms.TextBox();
            this.WhenceLabel = new System.Windows.Forms.Label();
            this.WhereTextBox = new System.Windows.Forms.TextBox();
            this.BookSiatButton = new System.Windows.Forms.Button();
            this.WhereLabel = new System.Windows.Forms.Label();
            this.DateMonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.DateUpgradeCheckBox = new System.Windows.Forms.CheckBox();
            this.SearchTripsButton = new System.Windows.Forms.Button();
            this.BookingResultLabel = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ClassesDataGridView = new System.Windows.Forms.DataGridView();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiatsDataGridView = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TripsDataGridView = new System.Windows.Forms.DataGridView();
            this.Whence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Where = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepatureDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aircraft = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuPictureBox = new System.Windows.Forms.PictureBox();
            this.RightMenuPanel = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.RoleUserLabel = new System.Windows.Forms.Label();
            this.NameSurnameUserLabel = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.MyTripsPanelM = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.MyTripsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.BookingListPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.TripsStatistic = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1.SuspendLayout();
            this.CreateTripPanel.SuspendLayout();
            this.panel10.SuspendLayout();
            this.MyAirCompPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MyAirCompDataGridView)).BeginInit();
            this.MainPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClassesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiatsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TripsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuPictureBox)).BeginInit();
            this.RightMenuPanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel14.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.MyTripsPanelM.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.BookingListPanel.SuspendLayout();
            this.panel13.SuspendLayout();
            this.TripsStatistic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 694);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1167, 25);
            this.panel1.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Right;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(879, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(288, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "AirTicketsControle v.1.0.0";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CreateTripPanel
            // 
            this.CreateTripPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateTripPanel.Controls.Add(this.flowLayoutPanel2);
            this.CreateTripPanel.Controls.Add(this.panel10);
            this.CreateTripPanel.Location = new System.Drawing.Point(0, 21);
            this.CreateTripPanel.Name = "CreateTripPanel";
            this.CreateTripPanel.Size = new System.Drawing.Size(1167, 684);
            this.CreateTripPanel.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.AutoScroll = true;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(324, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(840, 678);
            this.flowLayoutPanel2.TabIndex = 13;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gainsboro;
            this.panel10.Controls.Add(this.label17);
            this.panel10.Controls.Add(this.label16);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Controls.Add(this.ArrivalDateTimePicker1);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Controls.Add(this.label12);
            this.panel10.Controls.Add(this.DepatureDateTimePicker);
            this.panel10.Controls.Add(this.CreateTripButton);
            this.panel10.Controls.Add(this.AirCompaniesComboBox);
            this.panel10.Controls.Add(this.CreateTripAddClassButton);
            this.panel10.Controls.Add(this.CreateTripWhenceTextBox);
            this.panel10.Controls.Add(this.CreateTripWhereTextBox);
            this.panel10.Controls.Add(this.CreateTripWhenceLabel);
            this.panel10.Controls.Add(this.CreateTripWhereLabel);
            this.panel10.Controls.Add(this.CreateTripAircraftTextBox);
            this.panel10.Controls.Add(this.CrateTripAircraftLabel);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(304, 684);
            this.panel10.TabIndex = 12;
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(0, 466);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(304, 2);
            this.label17.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(11, 316);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 17);
            this.label16.TabIndex = 13;
            this.label16.Text = "Arrival date";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(9, 255);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 17);
            this.label15.TabIndex = 12;
            this.label15.Text = "Depature date";
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(11, 546);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(279, 98);
            this.label14.TabIndex = 11;
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ArrivalDateTimePicker1
            // 
            this.ArrivalDateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ArrivalDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ArrivalDateTimePicker1.Location = new System.Drawing.Point(9, 336);
            this.ArrivalDateTimePicker1.Name = "ArrivalDateTimePicker1";
            this.ArrivalDateTimePicker1.Size = new System.Drawing.Size(281, 27);
            this.ArrivalDateTimePicker1.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(9, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Air companies";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(8, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 29);
            this.label12.TabIndex = 9;
            this.label12.Text = "Create trip";
            // 
            // DepatureDateTimePicker
            // 
            this.DepatureDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DepatureDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DepatureDateTimePicker.Location = new System.Drawing.Point(9, 275);
            this.DepatureDateTimePicker.Name = "DepatureDateTimePicker";
            this.DepatureDateTimePicker.Size = new System.Drawing.Size(281, 27);
            this.DepatureDateTimePicker.TabIndex = 4;
            // 
            // CreateTripButton
            // 
            this.CreateTripButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.CreateTripButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CreateTripButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateTripButton.Location = new System.Drawing.Point(11, 487);
            this.CreateTripButton.Name = "CreateTripButton";
            this.CreateTripButton.Size = new System.Drawing.Size(281, 38);
            this.CreateTripButton.TabIndex = 7;
            this.CreateTripButton.Text = "Create trip";
            this.CreateTripButton.UseVisualStyleBackColor = false;
            this.CreateTripButton.Click += new System.EventHandler(this.CreateTripButton_Click);
            // 
            // AirCompaniesComboBox
            // 
            this.AirCompaniesComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AirCompaniesComboBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.AirCompaniesComboBox.FormattingEnabled = true;
            this.AirCompaniesComboBox.Location = new System.Drawing.Point(9, 75);
            this.AirCompaniesComboBox.Name = "AirCompaniesComboBox";
            this.AirCompaniesComboBox.Size = new System.Drawing.Size(281, 28);
            this.AirCompaniesComboBox.TabIndex = 0;
            // 
            // CreateTripAddClassButton
            // 
            this.CreateTripAddClassButton.BackColor = System.Drawing.Color.PaleGreen;
            this.CreateTripAddClassButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CreateTripAddClassButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateTripAddClassButton.Location = new System.Drawing.Point(11, 414);
            this.CreateTripAddClassButton.Name = "CreateTripAddClassButton";
            this.CreateTripAddClassButton.Size = new System.Drawing.Size(281, 36);
            this.CreateTripAddClassButton.TabIndex = 6;
            this.CreateTripAddClassButton.Text = "Add class";
            this.CreateTripAddClassButton.UseVisualStyleBackColor = false;
            this.CreateTripAddClassButton.Click += new System.EventHandler(this.CreateTripAddClassButton_Click);
            // 
            // CreateTripWhenceTextBox
            // 
            this.CreateTripWhenceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTripWhenceTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.CreateTripWhenceTextBox.Location = new System.Drawing.Point(89, 119);
            this.CreateTripWhenceTextBox.Name = "CreateTripWhenceTextBox";
            this.CreateTripWhenceTextBox.Size = new System.Drawing.Size(201, 27);
            this.CreateTripWhenceTextBox.TabIndex = 1;
            // 
            // CreateTripWhereTextBox
            // 
            this.CreateTripWhereTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTripWhereTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.CreateTripWhereTextBox.Location = new System.Drawing.Point(89, 157);
            this.CreateTripWhereTextBox.Name = "CreateTripWhereTextBox";
            this.CreateTripWhereTextBox.Size = new System.Drawing.Size(201, 27);
            this.CreateTripWhereTextBox.TabIndex = 2;
            // 
            // CreateTripWhenceLabel
            // 
            this.CreateTripWhenceLabel.AutoSize = true;
            this.CreateTripWhenceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTripWhenceLabel.Location = new System.Drawing.Point(10, 124);
            this.CreateTripWhenceLabel.Name = "CreateTripWhenceLabel";
            this.CreateTripWhenceLabel.Size = new System.Drawing.Size(81, 17);
            this.CreateTripWhenceLabel.TabIndex = 3;
            this.CreateTripWhenceLabel.Text = "Whence : ";
            // 
            // CreateTripWhereLabel
            // 
            this.CreateTripWhereLabel.AutoSize = true;
            this.CreateTripWhereLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTripWhereLabel.Location = new System.Drawing.Point(10, 163);
            this.CreateTripWhereLabel.Name = "CreateTripWhereLabel";
            this.CreateTripWhereLabel.Size = new System.Drawing.Size(70, 17);
            this.CreateTripWhereLabel.TabIndex = 4;
            this.CreateTripWhereLabel.Text = "Where : ";
            // 
            // CreateTripAircraftTextBox
            // 
            this.CreateTripAircraftTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTripAircraftTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.CreateTripAircraftTextBox.Location = new System.Drawing.Point(89, 192);
            this.CreateTripAircraftTextBox.Name = "CreateTripAircraftTextBox";
            this.CreateTripAircraftTextBox.Size = new System.Drawing.Size(201, 27);
            this.CreateTripAircraftTextBox.TabIndex = 3;
            // 
            // CrateTripAircraftLabel
            // 
            this.CrateTripAircraftLabel.AutoSize = true;
            this.CrateTripAircraftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CrateTripAircraftLabel.Location = new System.Drawing.Point(10, 196);
            this.CrateTripAircraftLabel.Name = "CrateTripAircraftLabel";
            this.CrateTripAircraftLabel.Size = new System.Drawing.Size(76, 17);
            this.CrateTripAircraftLabel.TabIndex = 6;
            this.CrateTripAircraftLabel.Text = "Aircraft : ";
            // 
            // MyAirCompPanel
            // 
            this.MyAirCompPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MyAirCompPanel.BackColor = System.Drawing.Color.White;
            this.MyAirCompPanel.Controls.Add(this.chart4);
            this.MyAirCompPanel.Controls.Add(this.chart3);
            this.MyAirCompPanel.Controls.Add(this.dataGridView3);
            this.MyAirCompPanel.Controls.Add(this.panel2);
            this.MyAirCompPanel.Controls.Add(this.NameAirCompaniLabel);
            this.MyAirCompPanel.Controls.Add(this.MyAirCompDataGridView);
            this.MyAirCompPanel.Location = new System.Drawing.Point(0, 26);
            this.MyAirCompPanel.Name = "MyAirCompPanel";
            this.MyAirCompPanel.Size = new System.Drawing.Size(1164, 676);
            this.MyAirCompPanel.TabIndex = 7;
            this.MyAirCompPanel.Visible = false;
            // 
            // chart4
            // 
            chartArea1.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart4.Legends.Add(legend1);
            this.chart4.Location = new System.Drawing.Point(758, 6);
            this.chart4.Name = "chart4";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart4.Series.Add(series1);
            this.chart4.Size = new System.Drawing.Size(406, 370);
            this.chart4.TabIndex = 9;
            this.chart4.Text = "chart4";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title1.ForeColor = System.Drawing.SystemColors.HotTrack;
            title1.Name = "Title1";
            title1.Text = "Selling ticket";
            this.chart4.Titles.Add(title1);
            // 
            // chart3
            // 
            chartArea2.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart3.Legends.Add(legend2);
            this.chart3.Location = new System.Drawing.Point(357, 7);
            this.chart3.Name = "chart3";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart3.Series.Add(series2);
            this.chart3.Size = new System.Drawing.Size(410, 369);
            this.chart3.TabIndex = 8;
            this.chart3.Text = "chart3";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title2.ForeColor = System.Drawing.SystemColors.HotTrack;
            title2.Name = "Title1";
            title2.Text = "Profit";
            title2.ToolTip = "Profit";
            this.chart3.Titles.Add(title2);
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGridView3.Location = new System.Drawing.Point(563, 402);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(578, 256);
            this.dataGridView3.TabIndex = 7;
            this.dataGridView3.SelectionChanged += new System.EventHandler(this.dataGridView3_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Surname";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "email";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "phone";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Role";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.radioButton8);
            this.panel2.Controls.Add(this.radioButton7);
            this.panel2.Controls.Add(this.radioButton6);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.DescriptionAirCompTextBox);
            this.panel2.Controls.Add(this.CreateAirCompButton);
            this.panel2.Controls.Add(this.NameAirCompLabel);
            this.panel2.Controls.Add(this.GetMyAirCompButton);
            this.panel2.Controls.Add(this.LogoCreateAirCompLabel);
            this.panel2.Controls.Add(this.DescriptionAirCompLabel);
            this.panel2.Controls.Add(this.NameAirCompTextBox);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(357, 376);
            this.panel2.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(7, 311);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(337, 23);
            this.label18.TabIndex = 12;
            this.label18.Text = "Statistics setting";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(264, 341);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(80, 21);
            this.radioButton8.TabIndex = 11;
            this.radioButton8.Text = "6 month";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(144, 340);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(80, 21);
            this.radioButton7.TabIndex = 10;
            this.radioButton7.Text = "3 month";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Checked = true;
            this.radioButton6.Location = new System.Drawing.Point(9, 341);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(80, 21);
            this.radioButton6.TabIndex = 9;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "1 month";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.AutoSize = true;
            this.button7.BackColor = System.Drawing.Color.DodgerBlue;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(195, 263);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(148, 32);
            this.button7.TabIndex = 8;
            this.button7.Text = "Get my staff";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(7, 217);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(336, 38);
            this.label19.TabIndex = 7;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DescriptionAirCompTextBox
            // 
            this.DescriptionAirCompTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescriptionAirCompTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.DescriptionAirCompTextBox.Location = new System.Drawing.Point(9, 122);
            this.DescriptionAirCompTextBox.Name = "DescriptionAirCompTextBox";
            this.DescriptionAirCompTextBox.Size = new System.Drawing.Size(335, 27);
            this.DescriptionAirCompTextBox.TabIndex = 1;
            // 
            // CreateAirCompButton
            // 
            this.CreateAirCompButton.BackColor = System.Drawing.Color.PaleGreen;
            this.CreateAirCompButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateAirCompButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateAirCompButton.Location = new System.Drawing.Point(7, 166);
            this.CreateAirCompButton.Name = "CreateAirCompButton";
            this.CreateAirCompButton.Size = new System.Drawing.Size(336, 35);
            this.CreateAirCompButton.TabIndex = 2;
            this.CreateAirCompButton.Text = "Create";
            this.CreateAirCompButton.UseVisualStyleBackColor = false;
            this.CreateAirCompButton.Click += new System.EventHandler(this.CreateAirCompButton_Click);
            // 
            // NameAirCompLabel
            // 
            this.NameAirCompLabel.AutoSize = true;
            this.NameAirCompLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameAirCompLabel.Location = new System.Drawing.Point(10, 38);
            this.NameAirCompLabel.Name = "NameAirCompLabel";
            this.NameAirCompLabel.Size = new System.Drawing.Size(57, 20);
            this.NameAirCompLabel.TabIndex = 0;
            this.NameAirCompLabel.Text = "Name";
            // 
            // GetMyAirCompButton
            // 
            this.GetMyAirCompButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.GetMyAirCompButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GetMyAirCompButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetMyAirCompButton.Location = new System.Drawing.Point(8, 263);
            this.GetMyAirCompButton.Name = "GetMyAirCompButton";
            this.GetMyAirCompButton.Size = new System.Drawing.Size(160, 31);
            this.GetMyAirCompButton.TabIndex = 3;
            this.GetMyAirCompButton.Text = "Get my air companies";
            this.GetMyAirCompButton.UseVisualStyleBackColor = false;
            this.GetMyAirCompButton.Click += new System.EventHandler(this.GetMyAirCompButton_Click);
            // 
            // LogoCreateAirCompLabel
            // 
            this.LogoCreateAirCompLabel.AutoSize = true;
            this.LogoCreateAirCompLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogoCreateAirCompLabel.Location = new System.Drawing.Point(15, 5);
            this.LogoCreateAirCompLabel.Name = "LogoCreateAirCompLabel";
            this.LogoCreateAirCompLabel.Size = new System.Drawing.Size(194, 25);
            this.LogoCreateAirCompLabel.TabIndex = 5;
            this.LogoCreateAirCompLabel.Text = "Create air compani";
            // 
            // DescriptionAirCompLabel
            // 
            this.DescriptionAirCompLabel.AutoSize = true;
            this.DescriptionAirCompLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescriptionAirCompLabel.Location = new System.Drawing.Point(7, 95);
            this.DescriptionAirCompLabel.Name = "DescriptionAirCompLabel";
            this.DescriptionAirCompLabel.Size = new System.Drawing.Size(106, 20);
            this.DescriptionAirCompLabel.TabIndex = 1;
            this.DescriptionAirCompLabel.Text = "Description";
            // 
            // NameAirCompTextBox
            // 
            this.NameAirCompTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameAirCompTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.NameAirCompTextBox.Location = new System.Drawing.Point(9, 64);
            this.NameAirCompTextBox.Name = "NameAirCompTextBox";
            this.NameAirCompTextBox.Size = new System.Drawing.Size(335, 27);
            this.NameAirCompTextBox.TabIndex = 0;
            // 
            // NameAirCompaniLabel
            // 
            this.NameAirCompaniLabel.AutoSize = true;
            this.NameAirCompaniLabel.Location = new System.Drawing.Point(4, 16);
            this.NameAirCompaniLabel.Name = "NameAirCompaniLabel";
            this.NameAirCompaniLabel.Size = new System.Drawing.Size(0, 17);
            this.NameAirCompaniLabel.TabIndex = 1;
            // 
            // MyAirCompDataGridView
            // 
            this.MyAirCompDataGridView.AllowUserToAddRows = false;
            this.MyAirCompDataGridView.AllowUserToDeleteRows = false;
            this.MyAirCompDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MyAirCompDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MyAirCompDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.MyAirCompDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MyAirCompDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameAirComp,
            this.DescriptionAirComp});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MyAirCompDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.MyAirCompDataGridView.Location = new System.Drawing.Point(16, 402);
            this.MyAirCompDataGridView.Name = "MyAirCompDataGridView";
            this.MyAirCompDataGridView.ReadOnly = true;
            this.MyAirCompDataGridView.RowTemplate.Height = 24;
            this.MyAirCompDataGridView.Size = new System.Drawing.Size(482, 256);
            this.MyAirCompDataGridView.TabIndex = 3;
            this.MyAirCompDataGridView.SelectionChanged += new System.EventHandler(this.MyAirCompDataGridView_SelectionChanged);
            // 
            // NameAirComp
            // 
            this.NameAirComp.HeaderText = "Name";
            this.NameAirComp.Name = "NameAirComp";
            this.NameAirComp.ReadOnly = true;
            // 
            // DescriptionAirComp
            // 
            this.DescriptionAirComp.HeaderText = "Description";
            this.DescriptionAirComp.Name = "DescriptionAirComp";
            this.DescriptionAirComp.ReadOnly = true;
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanel.BackColor = System.Drawing.Color.White;
            this.MainPanel.Controls.Add(this.panel3);
            this.MainPanel.Controls.Add(this.BookingResultLabel);
            this.MainPanel.Controls.Add(this.panel5);
            this.MainPanel.Location = new System.Drawing.Point(0, 29);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1166, 676);
            this.MainPanel.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.label42);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.BuyTicketMainPanelButton);
            this.panel3.Controls.Add(this.DescrAirCompaniMainPanelTextBox);
            this.panel3.Controls.Add(this.NameAirCompaniMainPanelTextBox);
            this.panel3.Controls.Add(this.BookingResultLabel1);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.WhenceTextBox);
            this.panel3.Controls.Add(this.WhenceLabel);
            this.panel3.Controls.Add(this.WhereTextBox);
            this.panel3.Controls.Add(this.BookSiatButton);
            this.panel3.Controls.Add(this.WhereLabel);
            this.panel3.Controls.Add(this.DateMonthCalendar);
            this.panel3.Controls.Add(this.DateUpgradeCheckBox);
            this.panel3.Controls.Add(this.SearchTripsButton);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(345, 676);
            this.panel3.TabIndex = 13;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(10, 609);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(100, 23);
            this.label42.TabIndex = 21;
            this.label42.Text = "Passport";
            this.label42.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.textBox3.Location = new System.Drawing.Point(8, 635);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(331, 27);
            this.textBox3.TabIndex = 20;
            this.textBox3.Visible = false;
            // 
            // BuyTicketMainPanelButton
            // 
            this.BuyTicketMainPanelButton.BackColor = System.Drawing.Color.PaleGreen;
            this.BuyTicketMainPanelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BuyTicketMainPanelButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuyTicketMainPanelButton.Location = new System.Drawing.Point(5, 501);
            this.BuyTicketMainPanelButton.Name = "BuyTicketMainPanelButton";
            this.BuyTicketMainPanelButton.Size = new System.Drawing.Size(333, 33);
            this.BuyTicketMainPanelButton.TabIndex = 19;
            this.BuyTicketMainPanelButton.Text = "Buy";
            this.BuyTicketMainPanelButton.UseVisualStyleBackColor = false;
            this.BuyTicketMainPanelButton.Visible = false;
            this.BuyTicketMainPanelButton.Click += new System.EventHandler(this.BuyTicketMainPanelButton_Click);
            // 
            // DescrAirCompaniMainPanelTextBox
            // 
            this.DescrAirCompaniMainPanelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DescrAirCompaniMainPanelTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescrAirCompaniMainPanelTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.DescrAirCompaniMainPanelTextBox.Location = new System.Drawing.Point(5, 413);
            this.DescrAirCompaniMainPanelTextBox.Name = "DescrAirCompaniMainPanelTextBox";
            this.DescrAirCompaniMainPanelTextBox.Size = new System.Drawing.Size(333, 83);
            this.DescrAirCompaniMainPanelTextBox.TabIndex = 18;
            this.DescrAirCompaniMainPanelTextBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NameAirCompaniMainPanelTextBox
            // 
            this.NameAirCompaniMainPanelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameAirCompaniMainPanelTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameAirCompaniMainPanelTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.NameAirCompaniMainPanelTextBox.Location = new System.Drawing.Point(5, 374);
            this.NameAirCompaniMainPanelTextBox.Name = "NameAirCompaniMainPanelTextBox";
            this.NameAirCompaniMainPanelTextBox.Size = new System.Drawing.Size(333, 30);
            this.NameAirCompaniMainPanelTextBox.TabIndex = 17;
            this.NameAirCompaniMainPanelTextBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BookingResultLabel1
            // 
            this.BookingResultLabel1.BackColor = System.Drawing.Color.Gainsboro;
            this.BookingResultLabel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BookingResultLabel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.BookingResultLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookingResultLabel1.Location = new System.Drawing.Point(5, 546);
            this.BookingResultLabel1.Multiline = true;
            this.BookingResultLabel1.Name = "BookingResultLabel1";
            this.BookingResultLabel1.ReadOnly = true;
            this.BookingResultLabel1.Size = new System.Drawing.Size(333, 52);
            this.BookingResultLabel1.TabIndex = 16;
            this.BookingResultLabel1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(115, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Air compani";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(0, 338);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(345, 5);
            this.label3.TabIndex = 12;
            // 
            // WhenceTextBox
            // 
            this.WhenceTextBox.BackColor = System.Drawing.Color.Honeydew;
            this.WhenceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WhenceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WhenceTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.WhenceTextBox.Location = new System.Drawing.Point(80, 14);
            this.WhenceTextBox.Name = "WhenceTextBox";
            this.WhenceTextBox.Size = new System.Drawing.Size(252, 27);
            this.WhenceTextBox.TabIndex = 0;
            // 
            // WhenceLabel
            // 
            this.WhenceLabel.AutoSize = true;
            this.WhenceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WhenceLabel.Location = new System.Drawing.Point(8, 19);
            this.WhenceLabel.Name = "WhenceLabel";
            this.WhenceLabel.Size = new System.Drawing.Size(66, 17);
            this.WhenceLabel.TabIndex = 3;
            this.WhenceLabel.Text = "Whence";
            // 
            // WhereTextBox
            // 
            this.WhereTextBox.BackColor = System.Drawing.Color.Honeydew;
            this.WhereTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WhereTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WhereTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.WhereTextBox.Location = new System.Drawing.Point(80, 51);
            this.WhereTextBox.Name = "WhereTextBox";
            this.WhereTextBox.Size = new System.Drawing.Size(252, 27);
            this.WhereTextBox.TabIndex = 1;
            // 
            // BookSiatButton
            // 
            this.BookSiatButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.BookSiatButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BookSiatButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BookSiatButton.Location = new System.Drawing.Point(5, 507);
            this.BookSiatButton.Name = "BookSiatButton";
            this.BookSiatButton.Size = new System.Drawing.Size(333, 33);
            this.BookSiatButton.TabIndex = 11;
            this.BookSiatButton.Text = "Book sits";
            this.BookSiatButton.UseVisualStyleBackColor = false;
            this.BookSiatButton.Visible = false;
            this.BookSiatButton.Click += new System.EventHandler(this.BookSiatButton_Click);
            // 
            // WhereLabel
            // 
            this.WhereLabel.AutoSize = true;
            this.WhereLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WhereLabel.Location = new System.Drawing.Point(8, 56);
            this.WhereLabel.Name = "WhereLabel";
            this.WhereLabel.Size = new System.Drawing.Size(55, 17);
            this.WhereLabel.TabIndex = 4;
            this.WhereLabel.Text = "Where";
            // 
            // DateMonthCalendar
            // 
            this.DateMonthCalendar.BackColor = System.Drawing.Color.Black;
            this.DateMonthCalendar.Location = new System.Drawing.Point(5, 86);
            this.DateMonthCalendar.MaxSelectionCount = 1;
            this.DateMonthCalendar.Name = "DateMonthCalendar";
            this.DateMonthCalendar.TabIndex = 2;
            this.DateMonthCalendar.TrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // DateUpgradeCheckBox
            // 
            this.DateUpgradeCheckBox.AutoSize = true;
            this.DateUpgradeCheckBox.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateUpgradeCheckBox.Location = new System.Drawing.Point(249, 86);
            this.DateUpgradeCheckBox.Name = "DateUpgradeCheckBox";
            this.DateUpgradeCheckBox.Size = new System.Drawing.Size(83, 23);
            this.DateUpgradeCheckBox.TabIndex = 7;
            this.DateUpgradeCheckBox.Text = "Further";
            this.DateUpgradeCheckBox.UseVisualStyleBackColor = false;
            // 
            // SearchTripsButton
            // 
            this.SearchTripsButton.BackColor = System.Drawing.Color.PaleGreen;
            this.SearchTripsButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SearchTripsButton.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTripsButton.Location = new System.Drawing.Point(6, 298);
            this.SearchTripsButton.Name = "SearchTripsButton";
            this.SearchTripsButton.Size = new System.Drawing.Size(333, 33);
            this.SearchTripsButton.TabIndex = 5;
            this.SearchTripsButton.Text = "Search trips";
            this.SearchTripsButton.UseVisualStyleBackColor = false;
            this.SearchTripsButton.Click += new System.EventHandler(this.SearchTripsButton_Click);
            // 
            // BookingResultLabel
            // 
            this.BookingResultLabel.AutoSize = true;
            this.BookingResultLabel.Location = new System.Drawing.Point(755, 427);
            this.BookingResultLabel.Name = "BookingResultLabel";
            this.BookingResultLabel.Size = new System.Drawing.Size(0, 17);
            this.BookingResultLabel.TabIndex = 12;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.TripsDataGridView);
            this.panel5.Location = new System.Drawing.Point(367, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(788, 676);
            this.panel5.TabIndex = 15;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 310);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(788, 34);
            this.panel6.TabIndex = 15;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(477, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(311, 34);
            this.panel7.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(-5, -1);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 300, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sits";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.ForestGreen;
            this.label1.Location = new System.Drawing.Point(-4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ClassesDataGridView);
            this.panel4.Controls.Add(this.SiatsDataGridView);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 344);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(788, 332);
            this.panel4.TabIndex = 14;
            // 
            // ClassesDataGridView
            // 
            this.ClassesDataGridView.AllowUserToAddRows = false;
            this.ClassesDataGridView.AllowUserToDeleteRows = false;
            this.ClassesDataGridView.AllowUserToResizeColumns = false;
            this.ClassesDataGridView.AllowUserToResizeRows = false;
            this.ClassesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClassesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ClassesDataGridView.BackgroundColor = System.Drawing.Color.LightGray;
            this.ClassesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Name,
            this.Price});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ClassesDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.ClassesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.ClassesDataGridView.Name = "ClassesDataGridView";
            this.ClassesDataGridView.ReadOnly = true;
            this.ClassesDataGridView.RowTemplate.Height = 24;
            this.ClassesDataGridView.Size = new System.Drawing.Size(400, 311);
            this.ClassesDataGridView.TabIndex = 0;
            this.ClassesDataGridView.SelectionChanged += new System.EventHandler(this.ClassesDataGridView_SelectionChanged);
            // 
            // Name
            // 
            this.Name.HeaderText = "Name";
            this.Name.Name = "Name";
            this.Name.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // SiatsDataGridView
            // 
            this.SiatsDataGridView.AllowUserToAddRows = false;
            this.SiatsDataGridView.AllowUserToDeleteRows = false;
            this.SiatsDataGridView.AllowUserToResizeColumns = false;
            this.SiatsDataGridView.AllowUserToResizeRows = false;
            this.SiatsDataGridView.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.SiatsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SiatsDataGridView.BackgroundColor = System.Drawing.Color.LightGray;
            this.SiatsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SiatsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SiatsDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.SiatsDataGridView.Location = new System.Drawing.Point(477, 0);
            this.SiatsDataGridView.Name = "SiatsDataGridView";
            this.SiatsDataGridView.ReadOnly = true;
            this.SiatsDataGridView.RowHeadersVisible = false;
            this.SiatsDataGridView.RowTemplate.Height = 24;
            this.SiatsDataGridView.Size = new System.Drawing.Size(311, 311);
            this.SiatsDataGridView.TabIndex = 10;
            this.SiatsDataGridView.SelectionChanged += new System.EventHandler(this.SiatsDataGridView_SelectionChanged);
            // 
            // Number
            // 
            this.Number.HeaderText = "Number";
            this.Number.Name = "Number";
            this.Number.ReadOnly = true;
            // 
            // TripsDataGridView
            // 
            this.TripsDataGridView.AllowUserToAddRows = false;
            this.TripsDataGridView.AllowUserToDeleteRows = false;
            this.TripsDataGridView.AllowUserToResizeColumns = false;
            this.TripsDataGridView.AllowUserToResizeRows = false;
            this.TripsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TripsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TripsDataGridView.BackgroundColor = System.Drawing.Color.LightGray;
            this.TripsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TripsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Whence,
            this.Where,
            this.DepatureDate,
            this.ArrivalDate,
            this.Aircraft});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TripsDataGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.TripsDataGridView.Location = new System.Drawing.Point(0, 18);
            this.TripsDataGridView.Name = "TripsDataGridView";
            this.TripsDataGridView.ReadOnly = true;
            this.TripsDataGridView.RowTemplate.Height = 24;
            this.TripsDataGridView.Size = new System.Drawing.Size(788, 255);
            this.TripsDataGridView.TabIndex = 8;
            this.TripsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TripsDataGridView_CellContentClick);
            this.TripsDataGridView.SelectionChanged += new System.EventHandler(this.TripsDataGridView_SelectionChanged);
            // 
            // Whence
            // 
            this.Whence.HeaderText = "Whence";
            this.Whence.Name = "Whence";
            this.Whence.ReadOnly = true;
            this.Whence.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Where
            // 
            this.Where.HeaderText = "Where";
            this.Where.Name = "Where";
            this.Where.ReadOnly = true;
            // 
            // DepatureDate
            // 
            this.DepatureDate.HeaderText = "Depature date";
            this.DepatureDate.Name = "DepatureDate";
            this.DepatureDate.ReadOnly = true;
            // 
            // ArrivalDate
            // 
            this.ArrivalDate.HeaderText = "Arrival date";
            this.ArrivalDate.Name = "ArrivalDate";
            this.ArrivalDate.ReadOnly = true;
            // 
            // Aircraft
            // 
            this.Aircraft.HeaderText = "Aircraft";
            this.Aircraft.Name = "Aircraft";
            this.Aircraft.ReadOnly = true;
            // 
            // MenuPictureBox
            // 
            this.MenuPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.MenuPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuPictureBox.Image = global::Lilia_project.Properties.Resources.menuIcon;
            this.MenuPictureBox.Location = new System.Drawing.Point(1127, 3);
            this.MenuPictureBox.Name = "MenuPictureBox";
            this.MenuPictureBox.Size = new System.Drawing.Size(25, 24);
            this.MenuPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MenuPictureBox.TabIndex = 9;
            this.MenuPictureBox.TabStop = false;
            this.MenuPictureBox.Click += new System.EventHandler(this.button1_Click);
            // 
            // RightMenuPanel
            // 
            this.RightMenuPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RightMenuPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.RightMenuPanel.Controls.Add(this.label66);
            this.RightMenuPanel.Controls.Add(this.label65);
            this.RightMenuPanel.Controls.Add(this.tabControl1);
            this.RightMenuPanel.Controls.Add(this.label7);
            this.RightMenuPanel.Controls.Add(this.flowLayoutPanel1);
            this.RightMenuPanel.Controls.Add(this.panel8);
            this.RightMenuPanel.Location = new System.Drawing.Point(1185, 0);
            this.RightMenuPanel.Name = "RightMenuPanel";
            this.RightMenuPanel.Size = new System.Drawing.Size(342, 720);
            this.RightMenuPanel.TabIndex = 9;
            // 
            // label66
            // 
            this.label66.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label66.BackColor = System.Drawing.Color.PaleGreen;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label66.Location = new System.Drawing.Point(0, 428);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(345, 4);
            this.label66.TabIndex = 5;
            this.label66.Text = " ";
            // 
            // label65
            // 
            this.label65.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.ForeColor = System.Drawing.Color.ForestGreen;
            this.label65.Location = new System.Drawing.Point(8, 400);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(44, 24);
            this.label65.TabIndex = 4;
            this.label65.Text = "Info";
            this.label65.Visible = false;
            this.label65.Click += new System.EventHandler(this.label65_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 439);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(342, 257);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.Tag = "";
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.label60);
            this.tabPage1.Controls.Add(this.label59);
            this.tabPage1.Controls.Add(this.label58);
            this.tabPage1.Controls.Add(this.label57);
            this.tabPage1.Controls.Add(this.label56);
            this.tabPage1.Controls.Add(this.label49);
            this.tabPage1.Controls.Add(this.label48);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(334, 226);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Account";
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label60.Location = new System.Drawing.Point(119, 100);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(192, 23);
            this.label60.TabIndex = 9;
            this.label60.Text = " ";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label59.Location = new System.Drawing.Point(120, 78);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(192, 23);
            this.label59.TabIndex = 8;
            this.label59.Text = " ";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label58.Location = new System.Drawing.Point(79, 55);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(233, 23);
            this.label58.TabIndex = 7;
            this.label58.Text = " ";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label57.Location = new System.Drawing.Point(120, 32);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(192, 23);
            this.label57.TabIndex = 6;
            this.label57.Text = " ";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label56.Location = new System.Drawing.Point(120, 9);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(192, 24);
            this.label56.TabIndex = 5;
            this.label56.Text = " ";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.ForeColor = System.Drawing.Color.ForestGreen;
            this.label49.Location = new System.Drawing.Point(8, 102);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(91, 18);
            this.label49.TabIndex = 4;
            this.label49.Text = "Passport : ";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label48.ForeColor = System.Drawing.Color.ForestGreen;
            this.label48.Location = new System.Drawing.Point(8, 80);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(71, 18);
            this.label48.TabIndex = 3;
            this.label48.Text = "Phone : ";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.ForeColor = System.Drawing.Color.ForestGreen;
            this.label47.Location = new System.Drawing.Point(8, 57);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(65, 18);
            this.label47.TabIndex = 2;
            this.label47.Text = "Email : ";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.ForeColor = System.Drawing.Color.ForestGreen;
            this.label46.Location = new System.Drawing.Point(7, 35);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(90, 18);
            this.label46.TabIndex = 1;
            this.label46.Text = "Surname : ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.ForeColor = System.Drawing.Color.ForestGreen;
            this.label45.Location = new System.Drawing.Point(7, 11);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 18);
            this.label45.TabIndex = 0;
            this.label45.Text = "Name : ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel14);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.label61);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Controls.Add(this.label53);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.label51);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(334, 226);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Owner";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.AutoScroll = true;
            this.panel14.Controls.Add(this.label55);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(3, 129);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(328, 94);
            this.panel14.TabIndex = 11;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label55.Location = new System.Drawing.Point(10, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(0, 18);
            this.label55.TabIndex = 6;
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label64.Location = new System.Drawing.Point(94, 70);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(224, 23);
            this.label64.TabIndex = 10;
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label63.Location = new System.Drawing.Point(94, 49);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(224, 23);
            this.label63.TabIndex = 9;
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label62.Location = new System.Drawing.Point(94, 28);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(224, 23);
            this.label62.TabIndex = 8;
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label61.Location = new System.Drawing.Point(95, 7);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(224, 23);
            this.label61.TabIndex = 7;
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label54.ForeColor = System.Drawing.Color.ForestGreen;
            this.label54.Location = new System.Drawing.Point(101, 108);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(120, 18);
            this.label54.TabIndex = 5;
            this.label54.Text = "Air companies ";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label53.ForeColor = System.Drawing.Color.ForestGreen;
            this.label53.Location = new System.Drawing.Point(10, 77);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(71, 18);
            this.label53.TabIndex = 4;
            this.label53.Text = "Phone : ";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.ForeColor = System.Drawing.Color.ForestGreen;
            this.label52.Location = new System.Drawing.Point(10, 54);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(65, 18);
            this.label52.TabIndex = 3;
            this.label52.Text = "Email : ";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label51.ForeColor = System.Drawing.Color.ForestGreen;
            this.label51.Location = new System.Drawing.Point(9, 33);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(90, 18);
            this.label51.TabIndex = 2;
            this.label51.Text = "Surname : ";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.ForeColor = System.Drawing.Color.ForestGreen;
            this.label50.Location = new System.Drawing.Point(9, 11);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(67, 18);
            this.label50.TabIndex = 1;
            this.label50.Text = "Name : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.ForestGreen;
            this.label7.Location = new System.Drawing.Point(8, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 24);
            this.label7.TabIndex = 2;
            this.label7.Text = "Autorization";
            this.label7.Click += new System.EventHandler(this.autheticationToolStripMenuItem_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.label10);
            this.flowLayoutPanel1.Controls.Add(this.label11);
            this.flowLayoutPanel1.Controls.Add(this.label67);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(41, 188);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(298, 196);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Trips";
            this.label5.Click += new System.EventHandler(this.tripsToolStripMenuItem1_Click);
            this.label5.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(3, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 24);
            this.label6.TabIndex = 1;
            this.label6.Text = "My trips";
            this.label6.Visible = false;
            this.label6.Click += new System.EventHandler(this.myTripsToolStripMenuItem1_Click);
            this.label6.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(3, 68);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 24);
            this.label9.TabIndex = 3;
            this.label9.Text = "Air companies";
            this.label9.Visible = false;
            this.label9.Click += new System.EventHandler(this.airCompaniesToolStripMenuItem_Click);
            this.label9.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label9.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(3, 102);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 24);
            this.label10.TabIndex = 4;
            this.label10.Text = "Create trips";
            this.label10.Visible = false;
            this.label10.Click += new System.EventHandler(this.createTripToolStripMenuItem_Click);
            this.label10.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label10.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 136);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 0, 3, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 24);
            this.label11.TabIndex = 5;
            this.label11.Text = "Booking list";
            this.label11.Visible = false;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            this.label11.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label11.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(3, 170);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(124, 24);
            this.label67.TabIndex = 6;
            this.label67.Text = "Trips statistics";
            this.label67.Visible = false;
            this.label67.Click += new System.EventHandler(this.label67_Click);
            this.label67.MouseLeave += new System.EventHandler(this.label5_MouseLeave);
            this.label67.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label5_MouseHover);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.PaleGreen;
            this.panel8.Controls.Add(this.RoleUserLabel);
            this.panel8.Controls.Add(this.NameSurnameUserLabel);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(342, 136);
            this.panel8.TabIndex = 0;
            // 
            // RoleUserLabel
            // 
            this.RoleUserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RoleUserLabel.Location = new System.Drawing.Point(20, 89);
            this.RoleUserLabel.Name = "RoleUserLabel";
            this.RoleUserLabel.Size = new System.Drawing.Size(300, 25);
            this.RoleUserLabel.TabIndex = 1;
            this.RoleUserLabel.Text = "Guest";
            this.RoleUserLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NameSurnameUserLabel
            // 
            this.NameSurnameUserLabel.AutoSize = true;
            this.NameSurnameUserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameSurnameUserLabel.Location = new System.Drawing.Point(27, 29);
            this.NameSurnameUserLabel.MaximumSize = new System.Drawing.Size(340, 30);
            this.NameSurnameUserLabel.Name = "NameSurnameUserLabel";
            this.NameSurnameUserLabel.Size = new System.Drawing.Size(0, 25);
            this.NameSurnameUserLabel.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Controls.Add(this.MenuPictureBox);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1167, 32);
            this.panel9.TabIndex = 10;
            // 
            // MyTripsPanelM
            // 
            this.MyTripsPanelM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MyTripsPanelM.BackColor = System.Drawing.Color.White;
            this.MyTripsPanelM.Controls.Add(this.panel11);
            this.MyTripsPanelM.Controls.Add(this.MyTripsPanel);
            this.MyTripsPanelM.Location = new System.Drawing.Point(0, 29);
            this.MyTripsPanelM.Name = "MyTripsPanelM";
            this.MyTripsPanelM.Size = new System.Drawing.Size(1167, 670);
            this.MyTripsPanelM.TabIndex = 15;
            this.MyTripsPanelM.Visible = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Gainsboro;
            this.panel11.Controls.Add(this.label44);
            this.panel11.Controls.Add(this.label43);
            this.panel11.Controls.Add(this.label28);
            this.panel11.Controls.Add(this.label27);
            this.panel11.Controls.Add(this.label26);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.button2);
            this.panel11.Controls.Add(this.button1);
            this.panel11.Controls.Add(this.label25);
            this.panel11.Controls.Add(this.label24);
            this.panel11.Controls.Add(this.label23);
            this.panel11.Controls.Add(this.label22);
            this.panel11.Controls.Add(this.label21);
            this.panel11.Controls.Add(this.label20);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(338, 670);
            this.panel11.TabIndex = 8;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label44.Location = new System.Drawing.Point(119, 125);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(213, 23);
            this.label44.TabIndex = 13;
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(7, 125);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(107, 18);
            this.label43.TabIndex = 12;
            this.label43.Text = "Air compani :";
            // 
            // label28
            // 
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(6, 455);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(323, 85);
            this.label28.TabIndex = 11;
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.White;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Location = new System.Drawing.Point(0, 161);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(337, 4);
            this.label27.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(6, 179);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(148, 20);
            this.label26.TabIndex = 9;
            this.label26.Text = "Payment method";
            // 
            // panel12
            // 
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.radioButton5);
            this.panel12.Controls.Add(this.radioButton4);
            this.panel12.Controls.Add(this.radioButton3);
            this.panel12.Controls.Add(this.radioButton2);
            this.panel12.Controls.Add(this.radioButton1);
            this.panel12.Location = new System.Drawing.Point(6, 205);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(323, 203);
            this.panel12.TabIndex = 8;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(1, 114);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(110, 21);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "radioButton5";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(1, 87);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(110, 21);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "radioButton4";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(1, 61);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(110, 21);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "radioButton3";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(1, 32);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(110, 21);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(1, 4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(110, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.PaleGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(6, 414);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(323, 34);
            this.button2.TabIndex = 7;
            this.button2.Text = "Buy";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(17, 621);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(315, 34);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancel book";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.buttonDlete_Click);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(7, 91);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 23);
            this.label25.TabIndex = 5;
            this.label25.Text = "Price : ";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(6, 52);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 23);
            this.label24.TabIndex = 4;
            this.label24.Text = "Where : ";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(6, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 23);
            this.label23.TabIndex = 3;
            this.label23.Text = "Whence : ";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label22.Location = new System.Drawing.Point(89, 79);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(240, 37);
            this.label22.TabIndex = 2;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label21.Location = new System.Drawing.Point(85, 43);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(244, 37);
            this.label21.TabIndex = 1;
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label20.Location = new System.Drawing.Point(85, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(244, 34);
            this.label20.TabIndex = 0;
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MyTripsPanel
            // 
            this.MyTripsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MyTripsPanel.AutoScroll = true;
            this.MyTripsPanel.BackColor = System.Drawing.Color.White;
            this.MyTripsPanel.Location = new System.Drawing.Point(351, 1);
            this.MyTripsPanel.Name = "MyTripsPanel";
            this.MyTripsPanel.Size = new System.Drawing.Size(816, 669);
            this.MyTripsPanel.TabIndex = 7;
            // 
            // BookingListPanel
            // 
            this.BookingListPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BookingListPanel.BackColor = System.Drawing.Color.White;
            this.BookingListPanel.Controls.Add(this.flowLayoutPanel3);
            this.BookingListPanel.Controls.Add(this.panel13);
            this.BookingListPanel.Location = new System.Drawing.Point(0, 20);
            this.BookingListPanel.Name = "BookingListPanel";
            this.BookingListPanel.Size = new System.Drawing.Size(1167, 700);
            this.BookingListPanel.TabIndex = 16;
            this.BookingListPanel.Visible = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.Location = new System.Drawing.Point(428, 1);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(738, 678);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Gainsboro;
            this.panel13.Controls.Add(this.label41);
            this.panel13.Controls.Add(this.button5);
            this.panel13.Controls.Add(this.button4);
            this.panel13.Controls.Add(this.label40);
            this.panel13.Controls.Add(this.label39);
            this.panel13.Controls.Add(this.label38);
            this.panel13.Controls.Add(this.label37);
            this.panel13.Controls.Add(this.label36);
            this.panel13.Controls.Add(this.label35);
            this.panel13.Controls.Add(this.label34);
            this.panel13.Controls.Add(this.label33);
            this.panel13.Controls.Add(this.label32);
            this.panel13.Controls.Add(this.label31);
            this.panel13.Controls.Add(this.label30);
            this.panel13.Controls.Add(this.button3);
            this.panel13.Controls.Add(this.label29);
            this.panel13.Controls.Add(this.textBox2);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(403, 700);
            this.panel13.TabIndex = 0;
            // 
            // label41
            // 
            this.label41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(12, 473);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(379, 74);
            this.label41.TabIndex = 16;
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(216, 422);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(175, 35);
            this.button5.TabIndex = 15;
            this.button5.Text = "Cancel book";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.PaleGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(11, 422);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(186, 35);
            this.button4.TabIndex = 14;
            this.button4.Text = "Buy";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.White;
            this.label40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label40.Location = new System.Drawing.Point(0, 395);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(407, 10);
            this.label40.TabIndex = 13;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(16, 357);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 20);
            this.label39.TabIndex = 12;
            this.label39.Text = "Email : ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(15, 322);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 20);
            this.label38.TabIndex = 11;
            this.label38.Text = "Phone : ";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(13, 289);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(95, 20);
            this.label37.TabIndex = 10;
            this.label37.Text = "Surname : ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(13, 255);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 20);
            this.label36.TabIndex = 9;
            this.label36.Text = "Name : ";
            // 
            // label35
            // 
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label35.Location = new System.Drawing.Point(119, 356);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(272, 26);
            this.label35.TabIndex = 8;
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label34.Location = new System.Drawing.Point(119, 322);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(272, 26);
            this.label34.TabIndex = 7;
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label33.Location = new System.Drawing.Point(119, 286);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(272, 26);
            this.label33.TabIndex = 6;
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label32.Location = new System.Drawing.Point(119, 250);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(272, 26);
            this.label32.TabIndex = 5;
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(12, 82);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(379, 77);
            this.label31.TabIndex = 4;
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.White;
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Location = new System.Drawing.Point(-2, 229);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(407, 10);
            this.label30.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.PaleGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(10, 176);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(381, 35);
            this.button3.TabIndex = 2;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(12, 13);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(84, 20);
            this.label29.TabIndex = 1;
            this.label29.Text = "Passport";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.textBox2.Location = new System.Drawing.Point(11, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(380, 27);
            this.textBox2.TabIndex = 0;
            // 
            // TripsStatistic
            // 
            this.TripsStatistic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TripsStatistic.BackColor = System.Drawing.Color.White;
            this.TripsStatistic.Controls.Add(this.dataGridView1);
            this.TripsStatistic.Controls.Add(this.dataGridView2);
            this.TripsStatistic.Controls.Add(this.panel15);
            this.TripsStatistic.Controls.Add(this.chart2);
            this.TripsStatistic.Controls.Add(this.chart1);
            this.TripsStatistic.Location = new System.Drawing.Point(0, 26);
            this.TripsStatistic.Name = "TripsStatistic";
            this.TripsStatistic.Size = new System.Drawing.Size(1167, 673);
            this.TripsStatistic.TabIndex = 17;
            this.TripsStatistic.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.LightGray;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView1.Location = new System.Drawing.Point(708, 401);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(446, 253);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Price";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.LightGray;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.PaleGreen;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.Location = new System.Drawing.Point(13, 401);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(674, 253);
            this.dataGridView2.TabIndex = 10;
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Whence";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Where";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Depature date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Arrival date";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Aircraft";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // panel15
            // 
            this.panel15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel15.BackColor = System.Drawing.Color.Gainsboro;
            this.panel15.Controls.Add(this.label70);
            this.panel15.Controls.Add(this.button6);
            this.panel15.Controls.Add(this.checkBox2);
            this.panel15.Controls.Add(this.checkBox1);
            this.panel15.Controls.Add(this.monthCalendar1);
            this.panel15.Controls.Add(this.textBox5);
            this.panel15.Controls.Add(this.label69);
            this.panel15.Controls.Add(this.label68);
            this.panel15.Controls.Add(this.textBox4);
            this.panel15.Location = new System.Drawing.Point(2, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(316, 395);
            this.panel15.TabIndex = 2;
            // 
            // label70
            // 
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label70.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label70.Location = new System.Drawing.Point(6, 360);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(298, 24);
            this.label70.TabIndex = 8;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button6
            // 
            this.button6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button6.BackColor = System.Drawing.Color.PaleGreen;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(7, 315);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(297, 33);
            this.button6.TabIndex = 7;
            this.button6.Text = "Search";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox2.Location = new System.Drawing.Point(122, 292);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(104, 22);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "Last month";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox1.Location = new System.Drawing.Point(11, 292);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 22);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Futher";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(65, 76);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 4;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Honeydew;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox5.Location = new System.Drawing.Point(89, 41);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(215, 27);
            this.textBox5.TabIndex = 3;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label69.Location = new System.Drawing.Point(5, 44);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(65, 17);
            this.label69.TabIndex = 2;
            this.label69.Text = "Where :";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(5, 11);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(76, 17);
            this.label68.TabIndex = 1;
            this.label68.Text = "Whence :";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.Honeydew;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(89, 7);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(215, 27);
            this.textBox4.TabIndex = 0;
            // 
            // chart2
            // 
            chartArea3.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(732, 0);
            this.chart2.Name = "chart2";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart2.Series.Add(series3);
            this.chart2.Size = new System.Drawing.Size(435, 384);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            title3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title3.ForeColor = System.Drawing.SystemColors.HotTrack;
            title3.Name = "Title1";
            title3.Text = "Classes load";
            this.chart2.Titles.Add(title3);
            // 
            // chart1
            // 
            chartArea4.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            legend4.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(310, 1);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(425, 383);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title4.ForeColor = System.Drawing.SystemColors.HotTrack;
            title4.Name = "Title1";
            title4.Text = "Trips load";
            this.chart1.Titles.Add(title4);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 719);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.RightMenuPanel);
            this.Controls.Add(this.MyAirCompPanel);
            this.Controls.Add(this.BookingListPanel);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.MyTripsPanelM);
            this.Controls.Add(this.TripsStatistic);
            this.Controls.Add(this.CreateTripPanel);
            this.MinimumSize = new System.Drawing.Size(1185, 766);
            
            this.Text = "AirTickets";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.panel1.ResumeLayout(false);
            this.CreateTripPanel.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.MyAirCompPanel.ResumeLayout(false);
            this.MyAirCompPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MyAirCompDataGridView)).EndInit();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClassesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiatsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TripsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuPictureBox)).EndInit();
            this.RightMenuPanel.ResumeLayout(false);
            this.RightMenuPanel.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.MyTripsPanelM.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.BookingListPanel.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.TripsStatistic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.MonthCalendar DateMonthCalendar;
        private System.Windows.Forms.TextBox WhereTextBox;
        private System.Windows.Forms.TextBox WhenceTextBox;
        private System.Windows.Forms.Label WhereLabel;
        private System.Windows.Forms.Label WhenceLabel;
        private System.Windows.Forms.Button SearchTripsButton;
        private System.Windows.Forms.CheckBox DateUpgradeCheckBox;
        private System.Windows.Forms.DataGridView TripsDataGridView;
        private Label NameAirCompaniLabel;
        private DataGridView ClassesDataGridView;
        private DataGridViewTextBoxColumn Name;
        private DataGridViewTextBoxColumn Price;
        private Button BookSiatButton;
        private DataGridView SiatsDataGridView;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn Whence;
        private DataGridViewTextBoxColumn Where;
        private DataGridViewTextBoxColumn DepatureDate;
        private DataGridViewTextBoxColumn ArrivalDate;
        private DataGridViewTextBoxColumn Aircraft;
        private Label BookingResultLabel;
        private Panel MyAirCompPanel;
        private DataGridView MyAirCompDataGridView;
        private DataGridViewTextBoxColumn NameAirComp;
        private DataGridViewTextBoxColumn DescriptionAirComp;
        private Button GetMyAirCompButton;
        private Panel CreateTripPanel;
        private Label CreateTripWhereLabel;
        private Label CreateTripWhenceLabel;
        private TextBox CreateTripWhereTextBox;
        private TextBox CreateTripWhenceTextBox;
        private DateTimePicker DepatureDateTimePicker;
        private DateTimePicker ArrivalDateTimePicker1;
        private Label CrateTripAircraftLabel;
        private TextBox CreateTripAircraftTextBox;
        private ComboBox AirCompaniesComboBox;
        private Button CreateTripAddClassButton;
        private Button CreateTripButton;
        private Label LogoCreateAirCompLabel;
        private Button CreateAirCompButton;
        private TextBox DescriptionAirCompTextBox;
        private TextBox NameAirCompTextBox;
        private Label DescriptionAirCompLabel;
        private Label NameAirCompLabel;
        private Panel panel3;
        private Panel panel5;
        private Panel panel4;
        private Panel panel6;
        private Panel panel7;
        private Label label2;
        private Label label1;
        private TextBox textBox1;
        private Label label3;
        private Label label4;
        private TextBox BookingResultLabel1;
        private Panel RightMenuPanel;
        private Panel panel9;
        private Panel panel8;
        private FlowLayoutPanel flowLayoutPanel1;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label NameSurnameUserLabel;
        private Label RoleUserLabel;
        private PictureBox MenuPictureBox;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label NameAirCompaniMainPanelTextBox;
        private Label DescrAirCompaniMainPanelTextBox;
        private Panel panel10;
        private Label label16;
        private Label label15;
        private Label label14;
        private Label label13;
        private Label label12;
        private FlowLayoutPanel flowLayoutPanel2;
        private Label label17;
        private Panel panel2;
        private Label label19;
        private Panel MyTripsPanelM;
        private FlowLayoutPanel MyTripsPanel;
        private Panel panel11;
        private Label label25;
        private Label label24;
        private Label label23;
        private Label label22;
        private Label label21;
        private Label label20;
        private Label label26;
        private Panel panel12;
        private RadioButton radioButton5;
        private RadioButton radioButton4;
        private RadioButton radioButton3;
        private RadioButton radioButton2;
        private RadioButton radioButton1;
        private Button button2;
        private Button button1;
        private Label label28;
        private Label label27;
        private Panel BookingListPanel;
        private Panel panel13;
        private Label label29;
        private TextBox textBox2;
        private FlowLayoutPanel flowLayoutPanel3;
        private Label label30;
        private Button button3;
        private Label label31;
        private Label label41;
        private Button button5;
        private Button button4;
        private Label label40;
        private Label label39;
        private Label label38;
        private Label label37;
        private Label label36;
        private Label label35;
        private Label label34;
        private Label label33;
        private Label label32;
        private Button BuyTicketMainPanelButton;
        private Label label8;
        private TextBox textBox3;
        private Label label42;
        private Label label44;
        private Label label43;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label60;
        private Label label59;
        private Label label58;
        private Label label57;
        private Label label56;
        private Label label49;
        private Label label48;
        private Label label47;
        private Label label46;
        private Label label45;
        private Label label64;
        private Label label63;
        private Label label62;
        private Label label61;
        private Label label55;
        private Label label54;
        private Label label53;
        private Label label52;
        private Label label51;
        private Label label50;
        private Label label65;
        private Label label66;
        private Panel panel14;
        private Panel TripsStatistic;
        private Label label67;
        private Panel panel15;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridView dataGridView2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private Button button6;
        private CheckBox checkBox2;
        private CheckBox checkBox1;
        private MonthCalendar monthCalendar1;
        private TextBox textBox5;
        private Label label69;
        private Label label68;
        private TextBox textBox4;
        private Label label70;
        private DataGridView dataGridView3;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private DataGridViewTextBoxColumn Column5;
        private Label label18;
        private RadioButton radioButton8;
        private RadioButton radioButton7;
        private RadioButton radioButton6;
        private Button button7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
    }
}