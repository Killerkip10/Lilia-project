﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lilia_project
{
    public enum Role
    {
        Owner,
        User,
        Manager,
        Paymaster,
        None
    }
    abstract class Config
    {
        public const string urlHost = "http://localhost";
        public const string urlPort = ":3000";
    }
}
