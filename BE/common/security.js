const jwt = require('jsonwebtoken');
const config = require('../config');

class Token{
    getToken(id, role){
        return jwt.sign({
            id: id,
            type: role
        }, config.jwt.secretKey)
    }
    checkRoleUser(req){
        if(req.cookies.token)
        try {
            console.log(req.cookies.token);
            req.type = jwt.verify(req.cookies.token, config.jwt.secretKey).type;
            req.id =  jwt.verify(req.cookies.token, config.jwt.secretKey).id;
        }catch(err) {
            console.error(err.message);
        }
    }
}
module.exports = new Token();