class Queries{
    getTripStat(req, res){
        require('./connection').request()
            .query(`
                SELECT COUNT(*) AS [empty]
                FROM [trips]
                JOIN [clases] ON [trips].[id] = [clases].[id_trip]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [siats].[occupied] = 0 AND [paid] = 0 AND [trips].[id] = ${req.params.id}
                
                SELECT COUNT(*) AS [booked]
                FROM [trips]
                JOIN [clases] ON [trips].[id] = [clases].[id_trip]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [siats].[occupied] = 1 AND [paid] = 0 AND [trips].[id] = ${req.params.id}   
                
                SELECT COUNT(*) AS [bought]
                FROM [trips]
                JOIN [clases] ON [trips].[id] = [clases].[id_trip]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [siats].[occupied] = 1 AND [paid] = 1 AND [trips].[id] = ${req.params.id}               
            `)
            .then(result=>{
                res.status(200).send([result.recordsets[0][0].empty, result.recordsets[1][0].booked, result.recordsets[2][0].bought]);
            })
            .catch(err=>{
               console.log(err.message);
               res.status(400).send();
            });
    }
    getClassStat(req, res){
        require('./connection').request()
            .query(`
                SELECT COUNT(*) AS [empty]
                FROM [clases]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [occupied] = 0 AND [paid] = 0 AND [clases].[id] = ${req.params.id}
                
                SELECT COUNT(*) AS [booked]
                FROM [clases]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [occupied] = 1 AND [paid] = 0 AND [clases].[id] = ${req.params.id}
                
                SELECT COUNT(*) AS [bought]
                FROM [clases]
                JOIN [siats] ON [siats].[id_class] = [clases].[id]
                WHERE [occupied] = 1 AND [paid] = 1 AND [clases].[id] = ${req.params.id}
            `)
            .then(result=>{
                res.status(200).send([result.recordsets[0][0].empty, result.recordsets[1][0].booked, result.recordsets[2][0].bought]);
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }
    getAirCompaniStat(req, res){
        require('./connection').request()
            .query(`
                SELECT SUM(price) AS [all]
                FROM [siats]
                JOIN [clases] ON [siats].[id_class] = [clases].[id]
                JOIN [trips] ON [clases].[id_trip] = [trips].[id]
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                WHERE 
                    [air_companies].[id] = ${req.query.id} 
                    AND [trips].[depature_date] <= '${this.getDateStat()}'
                    AND [trips].[depature_date] >= '${this.getDateStat(req.query.month)}'
                    
                SELECT SUM(price) AS [paid]
                FROM [siats]
                JOIN [clases] ON [siats].[id_class] = [clases].[id]
                JOIN [trips] ON [clases].[id_trip] = [trips].[id]
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                WHERE 
                    [air_companies].[id] = ${req.query.id} 
                    AND [siats].[paid] = 1
                    AND [trips].[depature_date] <= '${this.getDateStat()}'
                    AND [trips].[depature_date] >= '${this.getDateStat(req.query.month)}' 
            `)
            .then(result=>{
                res.status(200).send([result.recordsets[0][0].all || 0, result.recordsets[1][0].paid || 0]);
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }
    getStaffStat(req, res){
        require('./connection').request()
            .query(`
                SELECT COUNT(*) AS [all]
                FROM [siats]
                JOIN [clases] ON [siats].[id_class] = [clases].[id]
                JOIN [trips] ON [clases].[id_trip] = [trips].[id]
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                WHERE 
                    [air_companies].[id] = ${req.query.id} 
                    AND [trips].[depature_date] <= '${this.getDateStat()}'
                    AND [trips].[depature_date] >= '${this.getDateStat(req.query.month)}'
                    AND [siats].[paid] = 1
                    
                SELECT COUNT(*) AS [cashbox]
                FROM [siats]
                JOIN [clases] ON [siats].[id_class] = [clases].[id]
                JOIN [trips] ON [clases].[id_trip] = [trips].[id]
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                WHERE 
                    [air_companies].[id] = ${req.query.id} 
                    AND [trips].[depature_date] <= '${this.getDateStat()}'
                    AND [trips].[depature_date] >= '${this.getDateStat(req.query.month)}'
                    AND [siats].[id_paymaster] IS NOT NULL
                    AND [siats].[paid] = 1
            `)
            .then(result=>{
                res.status(200).send([result.recordsets[0][0].all, result.recordsets[1][0].cashbox]);
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }

    getDateStat(month){
        let date;

        if(month){
            date = new Date();
            date = new Date(date.getFullYear(), date.getMonth() - month, date.getDate());
        }else{
            date = new Date();
        }
        return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`;
    }
}
module.exports = new Queries();