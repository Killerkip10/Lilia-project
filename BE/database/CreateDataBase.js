//Doesn't work, fix sits table


let sql = require('mssql');
let config = {
    user: 'vlad',
    password: '',
    server: 'localhost',
    database: 'Lilia-project',
};


sql.connect(config)
    .then(pool => {
        console.log('Connect to dataBase');

        pool.request()
            .query(`
                CREATE TABLE [users](
                    [id] INT PRIMARY KEY NOT NULL IDENTITY,
                    [login] NVARCHAR(30) NOT NULL,
                    [password] NVARCHAR(30) NOT NULL,
                    [name] NVARCHAR(30) NOT NULL,
                    [surname] NVARCHAR(30) NOT NULL,
                    [phone] NVARCHAR(30) NOT NULL,
                    [email] NVARCHAR(30)
                );
                CREATE TABLE [owners](
                    [id] INT PRIMARY KEY NOT NULL IDENTITY,
                    [login] NVARCHAR(30) NOT NULL,
                    [password] NVARCHAR(30) NOT NULL,
                    [name] NVARCHAR(30) NOT NULL,
                    [surname] NVARCHAR(30) NOT NULL,
                    [phone] NVARCHAR(30) NOT NULL,
                    [email] NVARCHAR(30)
                );
                CREATE TABLE [air_companies](
                    [id] INT PRIMARY KEY NOT NULL IDENTITY,
                    [id_owner] INT  NOT NULL,
                    [name] NVARCHAR(50) NOT NULL,
                    [discription] NVARCHAR(100) NOT NULL,
                    FOREIGN KEY ([id_owner]) REFERENCES [owners] ([id]) ON DELETE CASCADE
                );
                CREATE TABLE [trips](
                    [id] INT PRIMARY KEY NOT NULL IDENTITY,
                    [id_compani] INT NOT NULL,
                    [fromcity] NVARCHAR(100) NOT NULL,
                    [wherecity] NVARCHAR(100) NOT NULL,
                    [depature_date] DATETIME2(0) NOT NULL,
                    [arrival_date] DATETIME2(0) NOT NULL,
                    [aircraft] NVARCHAR(50) NOT NULL,
                    FOREIGN KEY ([id_compani]) REFERENCES [air_companies] ([id]) ON DELETE CASCADE
                );
                CREATE TABLE [clases](
                    [id] INT PRIMARY KEY NOT NULL IDENTITY,
                    [id_trip] INT NOT NULL,
                    [name] NVARCHAR(50) NOT NULL,
                    [price] FLOAT NOT NULL,
                    FOREIGN KEY ([id_trip]) REFERENCES [trips] ([id]) ON DELETE CASCADE
                );
                CREATE TABLE [siats](
                    [id_class] INT NOT NULL,
                    [id_user] INT,
                    [occupied] BIT DEFAULT 0,
                    FOREIGN KEY ([id_class]) REFERENCES [clases] ([id]) ON DELETE CASCADE,
                    FOREIGN KEY ([id_user]) REFERENCES [users] ([id])
                );
                
                
                INSERT INTO [users]
                VALUES
                ('1111', '1111', '${dates.RandomSelect(dates.names)}', '${dates.RandomSelect(dates.surnames)}', 
                '+375298645383', '${dates.RandomSelect(dates.emails)}'),
                
                ('user', 'user', '${dates.RandomSelect(dates.names)}', '${dates.RandomSelect(dates.surnames)}', 
                '111111111', '${dates.RandomSelect(dates.emails)}'),
                
                ('Killerkip10', '895623741', '${dates.RandomSelect(dates.names)}', '${dates.RandomSelect(dates.surnames)}', 
                '+2121332', '${dates.RandomSelect(dates.emails)}')
                             
                             
                INSERT INTO [owners]
                VALUES
                ('admin', 'admin', '${dates.RandomSelect(dates.names)}', '${dates.RandomSelect(dates.surnames)}', 
                '+375298645383', '${dates.RandomSelect(dates.emails)}'),
                
                ('1', '1234', '${dates.RandomSelect(dates.names)}', '${dates.RandomSelect(dates.surnames)}',
                '111111111', '${dates.RandomSelect(dates.emails)}')
                        
                                    
                INSERT INTO [air_companies]
                VALUES
                (1, '${dates.RandomSelect(dates.airCompanies)}', 'Very good compani'),
                (1, '${dates.RandomSelect(dates.airCompanies)}', 'Faster')
                  
                                    
                INSERT INTO [trips]
                VALUES
                ${dates.Randomtrips(100)}
                    
                                    
                INSERT INTO [clases]
                VALUES
                (1, 'Busines', 135.50),
                (1, 'Classic', 50),
                (1, 'Economic', 20)
                      
                                   
                DECLARE @num INT	
                SET @num = 40
                
                WHILE @num > 0
                    BEGIN
                        INSERT INTO [siats] (id_class) 
                        VALUES (1)
                        INSERT INTO [siats] (id_class) 
                        VALUES (2)
                        INSERT INTO [siats] (id_class) 
                        VALUES (3)
                        SET	@num = @num - 1
                    END
            `)
            .then(result=>{
                console.log('Tables created');
            })
            .catch(err=>{
                console.log(err);
            })
    })
    .catch(err =>{
        console.log(err);
    });

let dates ={
    names:['Vlad', 'Andrew', 'Maria', 'Pasha', 'Kostia', 'Djylik', 'Lilia'],
    surnames:['Pinchuk', 'Kashitski', 'Stelmah', 'Drozdov', 'Kurilkin'],
    towns:['Minsk', 'Moscow', 'Kiev', 'Washington D.C.', 'Paris'],
    airCompanies:['Belavia', 'Stels', 'Juniter', 'USA air compani', 'Terror'],
    emails:['vpinchuk2012@gmail.com', 'adg@mail.com', '3tweg@yandex.by'],

    RandomSelect: function(arr){
        return arr[Math.round(Math.random()*(arr.length - 1))];
    },
    RandomDate: function(){
        return new Date(2018, Math.round(Math.random()*10 + 1), Math.round(Math.random()*27 + 1), Math.round(Math.random()*24), Math.round(Math.random()*60), 0).toISOString();
    },
    Randomtrips(num){
        let queries = '';

            for(let i = 0; i < num; i++){
                queries += `(1, '${dates.RandomSelect(dates.towns)}', '${dates.RandomSelect(dates.towns)}','${dates.RandomDate()}', '${dates.RandomDate()}', 'Boing-324'),`;
            }

        queries = queries.substring(0, queries.length - 1);
        return queries;
    }
};
