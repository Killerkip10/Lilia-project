let sql = require('mssql');
let config = require('../config').dataConfig;

sql.connect(config)
    .then(pool => {
        console.log('Connect to dataBase');
        module.exports = pool;
    })
    .catch(err =>{
        console.log(err);
    });
