const token = require('../common/security');

module.exports = (req, res)=> {
    require('./connection').request()
        .query(`
            SELECT * FROM [users] 
            WHERE [login] = '${req.body.login || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS 
            AND [password] = '${req.body.password || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS
            
            SELECT * FROM [owners] 
            WHERE [login] = '${req.body.login || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS
            AND [password] = '${req.body.password || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS
            
            SELECT * FROM [staff]
            WHERE [login] = '${req.body.login || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS
            AND [password] = '${req.body.password || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS 
        `)
        .then(result=>{
            if(result.recordsets[0].length !== 0){
                res.cookie('token', token.getToken(result.recordsets[0][0].id, 1));
                res.send(result.recordsets[0][0]);
            }
            else if(result.recordsets[1].length !== 0){
                res.cookie('token', token.getToken(result.recordsets[1][0].id, 2));
                res.send(result.recordsets[1][0]);
            }
            else if(result.recordsets[2].length !== 0) {
                res.cookie('token', token.getToken(result.recordsets[2][0].id, result.recordsets[2][0].role));
                res.send(result.recordsets[2][0]);
            }else{
                res.send();
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(400).send();
        })
};
