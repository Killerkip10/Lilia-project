class Queries{
    getObjectBy(req, res, obj, param, id, t = true){
        require('./connection').request()
            .query(`
                SELECT * FROM [${obj}] 
                WHERE [${param}] = ${id}
            `)
            .then(result=>{
                if(t) {
                    res.status(200).send(result.recordset[0]);
                }else{
                    res.status(200).send(result.recordset);
                }
            })
            .catch(err=>{
                console.log(err.message);
                res.send();
            })
    }
    getTrips(req, res, params){
        require('./connection').request()
            .query(this.createQueryTrip(params))
            .then(result=>{
                res.status(200).send(result.recordset);
            })
            .catch(err=>{
               console.log(err.message);
               res.send();
            });
    }
    getTripsForStaff(req, res){
        require('./connection').request()
            .query(this.createQueryTripForStaff(req))
            .then(result=>{
                res.status(200).send(result.recordset);
            })
            .catch(err=>{
                console.log(err.message);
                res.send();
            });
    }
    getSitsShort(req, res, id){
        require('./connection').request()
            .query(`
                SELECT [id], [number] 
                FROM [siats]
                WHERE [id_class] = ${id}
                    AND [occupied] = 0
                    AND [paid] = 0
            `)
            .then(result=>{
                res.status(200).send(result.recordset);
            })
            .catch(err=>{
                console.log(err.message);
                res.send();
            })
    }
    bookSits(req, res, body){
        require('./connection').request()
            .query(this.createQueryBookSits(body))
            .then(result=>{
                if(result.recordset.length !== body.id_sit.length || body.id_sit.length === 0) {
                    res.status(400).send();
                }
                else{
                    return require('./connection').request()
                        .query(this.createQueryUpdateSits(body))
                        .then(result=>{
                            res.status(200).send();
                        })
                }
            })
            .catch(err=>{
                console.log(err.message);
                res.send();
            });
    }
    getMyTrips(req, res){
        require('./connection').request()
            .query(`
                SELECT 
                    siats.id, 
                    siats.number,
                    siats.paid, 
                    clases.name, 
                    clases.price,
                    trips.depature_date, 
                    trips.arrival_date, 
                    trips.fromcity, 
                    trips.wherecity,
                    [air_companies].[name] AS nameCompani
                FROM [siats]
                JOIN [clases] ON siats.id_class = clases.id
                JOIN [trips] ON clases.id_trip = trips.id
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                WHERE trips.depature_date > '${this.getDate()}' AND siats.id_user = ${req.id}
            `)
            .then(result=>{
                res.status(200).send(result.recordset);
            })
            .catch(err=>{
                console.log(err.message);
                res.send();
            });
    }
    getUsersTrips(req, res){
        require('./connection').request()
            .query(`
                SELECT
                    siats.id, 
                    siats.number,
                    siats.paid, 
                    clases.name, 
                    clases.price, 
                    trips.depature_date, 
                    trips.arrival_date, 
                    trips.fromcity, 
                    trips.wherecity
                FROM [siats]
                JOIN [clases] ON siats.id_class = clases.id
                JOIN [trips] ON clases.id_trip = trips.id
                JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id]
                JOIN [staff] ON [staff].[id_owner] = [air_companies].[id_owner]
                WHERE trips.depature_date > '${this.getDate()}' 
                    AND [siats].[id_user] = '${req.params.id}'
                    AND [staff].[id] = ${req.id}
            `)
            .then(result=>{
                res.status(200).send(result.recordset);
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }
    deleteBooking(req, res, id){
        require('./connection').request()
            .query(`
                UPDATE [siats]
                SET [occupied] = 0,
                [id_user] = NULL
                WHERE [id] = ${id} 
                    AND [id_user] = ${req.id}
                    AND [paid] = 0   
            `)
            .then(result=>{
                console.log(result);
                if(result.rowsAffected[0]) {
                    res.status(200).send();
                }else{
                    res.status(400).send();
                }
            })
            .catch(err=>{
                res.status(400).send();
            });
    }
    deleteBookingPaymaster(req, res){
        require('./connection').request()
            .query(`
                UPDATE [siats]
                SET [occupied] = 0,
                    [paid] = 0,  
                    [id_user] = NULL,
                    [id_paymaster] = NULL
                WHERE [id] = ${req.params.id}
                    AND ([paid] = 1 OR [occupied] = 1)
            `)
            .then(result=>{
                console.log(result);
                if(result.rowsAffected[0]) {
                    res.status(200).send();
                }else{
                    res.status(400).send();
                }
            })
            .catch(err=>{
                res.status(400).send();
            });
    }
    buyBooking(req, res){
        require('./connection').request()
            .query(`
                UPDATE [siats]
                SET [paid] = 1
                WHERE [id] = ${req.params.id}
                    AND [id_user] = ${req.id}
                    AND [paid] = 0
            `)
            .then(result=>{
                if(result.rowsAffected[0]){
                    res.status(200).send();
                }else{
                    res.status(400).send();
                }
            })
            .catch(err=>{
                res.status(400).send();
            });
    }
    buyTicket(req, res){
        require('./connection').request()
            .query(`
                UPDATE [siats]
                SET [paid] = 1,
                    [id_paymaster] = ${req.id}
                WHERE [id] = ${req.params.id}
                    AND [paid] = 0
            `)
            .then(result=>{
                console.log(result);
                if(result.rowsAffected[0]){
                    res.status(200).send();
                }else{
                    res.status(400).send();
                }
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }
    buyTickets(req, res){
        require('./connection').request()
            .query(this.createQueryBookSits(req.body))
            .then(result=>{
                if(result.recordset.length !== req.body.id_sit.length || req.body.id_sit.length === 0) {
                    res.status(400).send();
                }
                else{
                    return require('./connection').request()
                        .query(this.createQueryUpdateSitsPaymaster(req))
                        .then(result=>{
                            res.status(200).send();
                        })
                }
            })
            .catch(err=>{
                console.log(err);
            });
    }
    createAirCompani(req, res){
        require('./connection').request()
            .query(`
                SELECT [name] FROM [air_companies]
                WHERE [name] = '${req.body.name || ''}' COLLATE SQL_Latin1_General_CP1_CS_AS
            `)
            .then(result=>{
                if(result.recordset.length) {
                    res.status(400).send();
                }else{
                    return require('./connection').request()
                        .query(`
                            INSERT INTO [air_companies]
                            VALUES ('${req.id}', '${req.body.name}', '${req.body.description}')
                        `)
                        .then(result=>{
                            res.status(200).send();
                        })
                }
            })
            .catch(err=>{
                res.status(400).send();
            });
    }
    createTrip(req, res){
        require('./connection').request()
            .query(`
                DECLARE @IdentityTrip INT
                DECLARE @IdentityClass INT
                DECLARE @num INT    
                                
                INSERT INTO [trips] 
                VALUES (
                    ${req.body.trip.id_compani}, 
                    '${req.body.trip.fromcity}', 
                    '${req.body.trip.wherecity}', 
                    '${this.getFullDate(req.body.trip.depature_date)}', 
                    '${this.getFullDate(req.body.trip.arrival_date)}', 
                    '${req.body.trip.aircraft}') 
                SET @IdentityTrip = SCOPE_IDENTITY()  
                
                ${this.createQueryInsertsClasses(req.body.classes)}                      
            `)
            .then(result=>{
                res.status(200).send();
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
        console.log(req.body);
        res.send();
    }
    getAirCompanies(req, res){
        require('./connection').request()
            .query(`
                SELECT 
                    [air_companies].[id], 
                    [air_companies].[id_owner], 
                    [air_companies].[name], 
                    [air_companies].[discription]
                FROM [air_companies]
                JOIN [staff] ON [air_companies].[id_owner] = [staff].[id_owner]
                WHERE [staff].[id] = ${req.id}
            `)
            .then(result=>{
                res.send(result.recordset);
            })
            .catch(err=>{
                console.log(err.message);
                res.status(400).send();
            });
    }
    getOwner(req, res){
        require('./connection').request()
            .query(`
                SELECT DISTINCT
                    [owners].[id],
                    [owners].[name],
                    [owners].[surname],
                    [owners].[email],
                    [owners].[phone]
                FROM [owners]
                JOIN [staff] ON [owners].[id] = [staff].[id_owner]
                WHERE [staff].[id] = ${req.id}
                
                SELECT DISTINCT [air_companies].[name] AS name
                FROM [air_companies]
                JOIN [staff] ON [staff].[id_owner] = [air_companies].[id_owner]
                WHERE [staff].[id] = ${req.id}
            `)
            .then(result=>{
                if(result.recordsets[0][0])
                    result.recordsets[0][0].companies = result.recordsets[1].map((item) => item.name);
                res.status(200).send(result.recordsets[0][0]);
            })
            .catch(err=>{
                console.log(err);
                res.status(400).send();
            });
    }

    getDate(date, sub){
        if(date){
            let arrDate = date.split('.');
            date = new Date(arrDate[2], arrDate[1] - 1, arrDate[0]);

                if(date < new Date()){
                    date = new Date();
                }
        }else if(sub){
            date = new Date();
            date = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
        }else{
            date = new Date();
        }
        return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`;
    }
    getFullDate(date){
        let dateTime = date.split(' ');
        let externalDate = dateTime[0].split('.');

        date = new Date(externalDate[2], externalDate[1] - 1, externalDate[0]);
        return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()} ${dateTime[1]}`;
    }
    createQueryTrip(params){
        let sql = `SELECT * FROM [trips] `;

            if(params.exact === '0'){
                sql += ` WHERE CAST([depature_date] AS DATE) = '${this.getDate(params.date)}'`;
            }
            else {
                sql += ` WHERE CAST([depature_date] AS DATE) >= '${this.getDate(params.date)}'`;
            }
            if(params.whencecity){
                sql += ` AND [fromcity] = '${params.whencecity}'`;
            }
            if(params.wherecity){
                sql += ` AND [wherecity] = '${params.wherecity}'`;
            }

        return sql;
    }
    createQueryTripForStaff(req){
        let sql = `SELECT DISTINCT
                        [trips].[id], 
                        [id_compani], 
                        [fromcity], 
                        [wherecity], 
                        [depature_date],
                        [arrival_date], 
                        [aircraft] 
                    FROM [trips] 
                    JOIN [air_companies] ON [trips].[id_compani] = [air_companies].[id] 
                    JOIN [staff] ON [staff].[id_owner] = [air_companies].[id_owner]
                    `;

        console.log(req.query);
        if(req.query.exact === '1' && req.query.alltime === '0'){
            sql += ` WHERE CAST([depature_date] AS DATE) >= '${this.getDate(req.query.date)}'`;
        }
        else if(req.query.alltime === '1'){
            sql += ` WHERE CAST([depature_date] AS DATE) >= '${this.getDate('', true)}'`;
        }else{
            sql += ` WHERE CAST([depature_date] AS DATE) = '${this.getDate(req.query.date)}'`;
        }
        if(req.query.whencecity){
            sql += ` AND [fromcity] = '${req.query.whencecity}'`;
        }
        if(req.query.wherecity){
            sql += ` AND [wherecity] = '${req.query.wherecity}'`;
        }
        sql += ` AND ([staff].[id] = ${req.id} OR [staff].[id_owner] = ${req.id})`;

        return sql;
    }
    createQueryBookSits(obj){
        let sql = `SELECT [occupied] FROM [siats] WHERE ([id] = ${obj.id_sit[0]} `;

            for(let i = 1; i < obj.id_sit.length; i++){

                sql += `OR [id] = ${obj.id_sit[i]} `;
            }

        sql += `) AND [occupied] = 0`;

        return sql;
    }
    createQueryUpdateSits(obj){
        let sql = `UPDATE [siats] SET [occupied] = 1, [id_user] = ${obj.id_user} WHERE [id] = ${obj.id_sit[0]} `;

            for(let i = 1; i < obj.id_sit.length; i++){
                sql += `OR [id] = ${obj.id_sit[i]} `
            }

        return sql;
    }
    createQueryUpdateSitsPaymaster(req){
        let sql = `UPDATE [siats] SET [occupied] = 1, [paid] = 1, [id_paymaster] = ${req.id} WHERE [id] = ${req.body.id_sit[0]} `;

        for(let i = 1; i < req.body.id_sit.length; i++){
            sql += `OR [id] = ${req.body.id_sit[i]} `
        }

        return sql;
    }
    createQueryInsertsClasses(obj){
        let sql = ``;

            for(let i = 0; i < obj.length; i++){
                sql += `INSERT INTO [clases] VALUES  (@IdentityTrip, '${obj[i].name}', '${obj[i].price}')`;
                sql += ` 
                    
                    SET @num = 1
                    SET @IdentityClass = SCOPE_IDENTITY()
                    
                    WHILE @num <= ${obj[i].siats}
                        BEGIN
                            INSERT INTO [siats] (id_class, number) 
                            VALUES (@IdentityClass, @num)
                            SET	@num = @num + 1
                        END
                    
                `
            }

        return sql;
    }
}
module.exports = new Queries();