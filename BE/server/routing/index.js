const token = require('../../common/security') ;

module.exports = (app)=>{
    app.use('/auth', require('./authReg'));

    app.use((req, res, next)=>{
        token.checkRoleUser(req);
        next();
    });

    app.use('/data', require('./data'));
};
