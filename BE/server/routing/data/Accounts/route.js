const router = require('express').Router();
const query = require('../../../../database/queriesDB');

router.route('/passport/:passport')
    .get((req, res)=>{
        query.getObjectBy(req, res, 'users', 'passport', `'${req.params.passport}'`);
    });
router.route('/owner')
    .get((req, res)=>{
        query.getOwner(req, res);
    });

router.use((req, res, next)=>{
   if(req.type === 2){
       next();
   } else{
       res.status(401).send();
   }
});
router.route('/staff')
    .get((req, res)=>{
        query.getObjectBy(req, res, 'staff', 'id_owner', req.id, false);
    });


module.exports = router;