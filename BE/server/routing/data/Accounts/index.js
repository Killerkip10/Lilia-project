const router = require('express').Router();

router.use((req, res, next)=>{
   if(req.type!== 1 && req.type){
       next();
   } else{
       res.status(401).send();
   }
});
router.use(require('./route'));

module.exports = router;