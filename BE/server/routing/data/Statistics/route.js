const router = require('express').Router();
const query = require('../../../../database/queryStatisticsDB');

router.route('/trip/:id')
    .get((req, res)=>{
        query.getTripStat(req, res);
    });
router.route('/class/:id')
    .get((req, res)=>{
        query.getClassStat(req, res);
    });

router.use((req, res, next)=>{
   if(req.type === 2){
       next();
   }else{
       res.status(401).send();
   }
});
router.route('/aircompani')
    .get((req, res)=>{
        query.getAirCompaniStat(req, res);
    });
router.route('/staff')
    .get((req, res)=>{
        query.getStaffStat(req, res);
    });

module.exports = router;