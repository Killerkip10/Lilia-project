const router = require('express').Router();

router.use((req, res, next)=>{
   if(req.type === 2 || req.type === 3){
       next();
   } else{
       res.status(401).send();
   }
});
router.use(require('./route'));

module.exports = router;