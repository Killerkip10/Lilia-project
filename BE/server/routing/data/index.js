const route = require('express').Router();

route.use('/aircompani', require('./AirCompanies'));
route.use('/trip', require('./Trips'));
route.use('/account', require('./Accounts'));
route.use('/statistics', require('./Statistics'));

module.exports = route;