const router = require('express').Router();
const query = require('../../../../database/queriesDB');

router.route('/')
    .get((req, res)=>{
        if(!req.type || req.type === 1){
            query.getTrips(req, res, req.query);
        }else{
            query.getTripsForStaff(req, res);
        }
    });
router.use((req, res, next)=>{
    if(req.type) {
        next();
    }else {
        res.status(401).send();
    }
});
router.use(require('./route'));

module.exports = router;