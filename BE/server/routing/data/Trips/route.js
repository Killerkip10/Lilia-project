const router = require('express').Router();
const query = require('../../../../database/queriesDB');

router.route('/create')
    .post((req, res)=>{
        if(req.type === 3) {
            query.createTrip(req, res);
        }else{
            res.status(401).send();
        }
    });
router.route('/user/:id')
    .get((req, res)=>{
        if(req.type === 4){
            query.getUsersTrips(req, res);
        }else{
            res.status(401).send();
        }
    });
router.route('/buy')
    .post((req, res)=>{
        if(req.type === 4){
            query.buyTickets(req, res);
        }else{
            res.status(401).send();
        }
    });
router.route('/buy/:id')
    .get((req, res)=>{
        if(req.type === 4){
            query.buyTicket(req, res);
        }else{
            res.status(401).send();
        }
    });
router.route('/deletepay/:id')
    .get((req, res)=>{
        if(req.type === 4){
            query.deleteBookingPaymaster(req, res);
        }else{
            res.status(401).send();
        }
    });

router.use((req, res, next)=>{
   if(req.type === 1){
       next();
   }else{
       res.status(401).send();
   }
});
router.route('/my')
    .get((req, res)=>{
        query.getMyTrips(req, res);
    });
router.route('/delete/:id')
    .get((req, res)=>{
        query.deleteBooking(req, res, req.params.id);
    });
router.route('/buybooking/:id')
    .get((req, res)=>{
        query.buyBooking(req, res);
    });
module.exports = router;