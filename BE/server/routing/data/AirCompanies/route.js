const router = require('express').Router();
const query = require('../../../../database/queriesDB');

router.route('/owner')
    .get((req, res)=>{
        query.getAirCompanies(req, res);
    });
router.route('/class/:id')
    .get((req, res)=>{
        query.getObjectBy(req, res, 'clases', 'id_trip', req.params.id, false);
    });
router.route('/sits/:id')
    .get((req, res)=>{
        query.getSitsShort(req, res, req.params.id);
    });
router.route('/get/:id')
    .get((req, res)=>{
        query.getObjectBy(req, res, 'air_companies', 'id',req.params.id);
    });
router.route('/create')
    .post((req, res) => {
        if(req.type === 2) {
            query.createAirCompani(req, res);
        }else{
            res.status(400).send();
        }
    });
router.route('/my')
    .get((req, res) => {
        if(req.type === 2) {
            query.getObjectBy(req, res, 'air_companies', 'id_owner', req.id, false);
        }else{
            res.status(401).send();
        }
    });

router.use((req, res, next)=>{
   if(req.type === 1){
       next();
   }
   else{
       res.status(401).send();
   }
});
router.route('/booksits')
    .post((req, res)=>{
        query.bookSits(req, res, req.body);
    });
module.exports = router;