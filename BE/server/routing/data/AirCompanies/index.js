const router= require('express').Router();
const route = require('./route');

//common

router.use((req, res, next)=>{
   if(req.type){
       next();
   }else{
       res.status(401).send();
   }
});
router.use(route);

module.exports = router;
