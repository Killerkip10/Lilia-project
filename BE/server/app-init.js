const config = require('../config.js');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParse = require('cookie-parser');
const morgan = require('morgan');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParse());
app.use(morgan('combined'));

require('../database/connection');
require('./routing/index.js')(app);

app.get('/', (req, res)=>{
    //Sends files
    res.send('main');
});

app.use((req, res, err)=>{
    if (err.status !== 404) console.error(err);
    res.sendStatus(err.status || 500);
});

app.listen(config.hostPort, ()=>{
    console.log(`Lilia project started on port ${config.hostPort}`);
});



